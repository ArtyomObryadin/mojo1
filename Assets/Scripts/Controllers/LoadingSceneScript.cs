﻿using System.Collections;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.SceneManagement;

namespace Wear.ARCompanyCore
{
    public class LoadingSceneScript : MonoBehaviour
    {
        [SerializeField] GameObject videoPlayer;
        public void Awake()
        {
            QualitySettings.antiAliasing = 2;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            StartCoroutine(LoadAsynchronously(1));
        }

        private void OnEnable()
        {
#if UNITY_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
            }
            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }
#endif
        }
#if UNITY_IOS
    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone); 
        //yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
    }
#endif
        IEnumerator LoadAsynchronously(int sceneIndex)
        {
            yield return new WaitForSeconds(2f);
            videoPlayer.SetActive(true);
            yield return new WaitForSeconds(3f);
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        }
    }
}
