﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Wear.ARCompanyCore.Managers;
using System.Linq;

namespace Wear.ARCompanyCore
{
    public class EffectController : MonoBehaviour
    {
        [SerializeField]
        private List<Effect> effects;

        private List<Animator> effAnimators;
        //private List<ParticleSystem> effPartSystems;
        private List<UnityWebRequestAsyncOperation> asyncReqModel;

        public List<Effect> Effects { get => effects; set => effects = value; }

        List<GameObject> effectsInScene;
        List<GameObject> screenObj;
        VideoContentController videoContr;
        bool videoIsReachEnd;

        private void OnEnable()
        {
            Debug.Log("EEEEENNANANANAAAABLED");
            AssetBundle.UnloadAllAssetBundles(true);
            asyncReqModel = new List<UnityWebRequestAsyncOperation>();
            videoContr = GetComponent<VideoContentController>();
            effectsInScene = new List<GameObject>();
            screenObj = new List<GameObject>();
            Effects = new List<Effect>();
            effAnimators = new List<Animator>();
            //effPartSystems = new List<ParticleSystem>();

            videoContr.OnVideoEndIsReached += HideEffects;
        }

        private void OnDisable()
        {
            //Caching.ClearCache();

            videoContr.OnVideoEndIsReached -= HideEffects;
        }

        private void OnDestroy()
        {
            AssetBundle.UnloadAllAssetBundles(true);
            Debug.Log("DEEESSTTTRROYYYY");
        }

        void Start() { }

        void Update()
        {
            if (Manager.ContentManager.IsVideoContentPlay && Effects.Count == effectsInScene.Count && !videoIsReachEnd)
            {
                int inSceneEffInd = 0;
                for (int i = 0; i < effectsInScene.Count; i++)
                {
                    if (Effects[i].timeline / 1000 <= videoContr.ClipLength)
                    {
                        inSceneEffInd = effectsInScene.FindIndex(obj => obj.name == Effects[i].effectDescription);
                        if (videoContr.CurrentClipTime >= (Effects[i].timeline / 1000) && !effectsInScene[inSceneEffInd].activeSelf)
                        {
                            if (Manager.ContentManager.CurrentVideoStopTime > (Effects[i].timeline / 1000) && !Effects[i].isPersistent && EndTimelineCheck(Effects[i].endtimeline))
                            {
                                continue;
                            }

                            if (Effects[i].timeline / 1000 + 0.2f <= videoContr.ClipLength)
                            {
                                effectsInScene[inSceneEffInd].SetActive(true);
                                //-----------------------------------------------------------
                                if (effAnimators[inSceneEffInd] != null)
                                {
                                    var stateInfo = effAnimators[inSceneEffInd].GetCurrentAnimatorStateInfo(0);
                                    Debug.Log(stateInfo.tagHash);
                                    Debug.Log(stateInfo.fullPathHash);
                                    Debug.Log(stateInfo.normalizedTime);

                                    var coef = videoContr.CurrentClipTime - Effects[i].timeline / 1000;
                                    if (coef > 0)
                                    {
                                        var animLength = effAnimators[inSceneEffInd].GetCurrentAnimatorStateInfo(0).length;
                                        var continueTime = (float)coef / animLength;

                                        effAnimators[inSceneEffInd].Play(stateInfo.fullPathHash, -1, continueTime);
                                    }
                                }
                                /*if (effPartSystems[inSceneEffInd] != null)
                                {
                                    var coef = videoContr.CurrentClipTime - Effects[i].timeline / 1000;
                                    if (coef > 0)
                                    {
                                        var animLength = effPartSystems[inSceneEffInd].duration;
                                        var continueTime = (float)coef / animLength;

                                        effPartSystems[inSceneEffInd].time = continueTime;
                                    }
                                }*/
                                //-----------------------------------------------------------
                            }
                        }

                        if (effectsInScene[inSceneEffInd].activeSelf && EndTimelineCheck(Effects[i].endtimeline))
                        {
                            effectsInScene[inSceneEffInd].SetActive(false);
                        }
                    }
                }
            }
            else
            {
                videoIsReachEnd = false;
            }
        }

        private bool EndTimelineCheck(float endTimeline)
        {
            return (endTimeline != 0f && endTimeline / 1000 <= videoContr.CurrentClipTime);
        }

        public void SpawnEffect()
        {
            try
            {
                if (Effects != null && Effects.Count > 0)
                {
                    for (int i = 0; i < Effects.Count; i++)
                    {
                        AssetBundle effectAsset = AssetBundle.LoadFromFile(Effects[i].effectPath);
                        Object assets = effectAsset.LoadAllAssetsAsync().asset;

                        if (assets != null)
                        {
                            Transform parent = Effects[i].isFullScreen ? Manager.ContentManager.ParentScreenObj : transform;

                            GameObject obj = Instantiate(assets, parent) as GameObject;
                            obj.SetActive(false);
                            effectsInScene.Add(obj);

                            effAnimators.Add(obj.GetComponentInChildren<Animator>(true));

                            //effPartSystems.Add(obj.GetComponentInChildren<ParticleSystem>(true));

                            if (Effects[i].isFullScreen)
                            {
                                screenObj.Add(obj);
                            }
                        }
                        else
                        {
                            Debug.Log("Element isn't GameObject");
                        }

                        effectAsset.Unload(false);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log("Bundle is broken and needed to redownload. Ecxeption " + e);
                //CheckBundleInstaniateProcess(false);
            }
        }

        private async void HideEffects()
        {
            for (int i = 0; i < effectsInScene?.Count; i++)
            {
                effectsInScene[i].SetActive(false);
            }

            //for (int i = 0; i < screenObj?.Count; i++) {
            //    Destroy(screenObj[i]);
            //}

            videoIsReachEnd = true;
        }


        int objCount = 0;

        public void SpawnAsync()
        {
            foreach (var model in Effects)
            {
                string loadURL = "file:///" + model.effectPath;
                //loadURL.Replace("http://", "file:///");
                //Debug.Log("=-=-=-==-=-= " + loadURL);

                UnityWebRequest reqModel = UnityWebRequestAssetBundle.GetAssetBundle(string.IsNullOrEmpty(loadURL) ? "EmptyUrl" : loadURL);
                reqModel.timeout = 25;
                Debug.LogFormat("<color=#00ff00ff>" + "TIME OUT OF REQUEST -" + reqModel.timeout + "</color>");

                reqModel.downloadHandler = new CustomAssetBundleDownloadHandler();

                UnityWebRequestAsyncOperation unityWebRequestAsyncOperation = reqModel.SendWebRequest();
                asyncReqModel.Add(unityWebRequestAsyncOperation);

                //Debug.Log("Add request to list " + asyncReqModel.Count);
                unityWebRequestAsyncOperation.completed += ComleteModelLoad;
            }
        }
        public void ComleteModelLoad(AsyncOperation operation)
        {
            StartCoroutine(LoadMainModelCar(operation));
        }

        IEnumerator LoadMainModelCar(AsyncOperation operation)
        {

            UnityWebRequestAsyncOperation req = operation as UnityWebRequestAsyncOperation;

            //Debug.LogFormat("<color=#ff0000ff>" + "!!!!!!!!!!Load URL!!!!!!!!!!- " + req.webRequest.url + "</color>");
            byte[] recievedData = ((CustomAssetBundleDownloadHandler)req.webRequest.downloadHandler).recivedData;

            string name = req.webRequest.url.Split('/').Last();

            int indexBundleInList = asyncReqModel.IndexOf(asyncReqModel.Find(x => x.Equals(req)));

            if (indexBundleInList < 0) { yield break; }

            AssetBundleRequest assetBundleRequest = new AssetBundleRequest();
            AssetBundleCreateRequest createRequest = AssetBundle.LoadFromMemoryAsync(recievedData);
            yield return createRequest;

            //Debug.LogFormat("<color=#00ff00ff> BUndleBytes </color>" + recievedData.Length);

            AssetBundle bundle = createRequest.assetBundle;
            if (bundle != null)
            {
                //Debug.Log(bundle.name);
                yield return assetBundleRequest = bundle.LoadAllAssetsAsync();
            }

            int currEff = Effects.FindIndex(elem => elem.effectDescription == name);

            Transform parent = Effects[currEff].isFullScreen ? Manager.ContentManager.ParentScreenObj : transform;

            GameObject obj = Instantiate(bundle == null ? new GameObject() : assetBundleRequest.asset, parent) as GameObject;

            obj.SetActive(false);
            obj.name = name;
            effectsInScene.Add(obj);

            effAnimators.Add(obj.GetComponentInChildren<Animator>(true));

            //effPartSystems.Add(obj.GetComponentInChildren<ParticleSystem>(true));

            if (Effects[currEff].isFullScreen)
            {
                screenObj.Add(obj);
            }
            //Debug.Log("*************************** " + name);
            bundle.Unload(false);

            objCount++;
            if (objCount == Effects.Count - 1)
            {
                objCount = 0;
                //isLoadComplete = true;
                asyncReqModel.Clear();
                asyncReqModel.TrimExcess();
            }
        }
    }

    class CustomAssetBundleDownloadHandler : DownloadHandlerScript
    {
        private string _targetFilePath;
        private System.IO.MemoryStream _fileStream;
        public byte[] recivedData;

        public CustomAssetBundleDownloadHandler()
        {
            //_targetFilePath = targetFilePath;
        }

        protected override bool ReceiveData(byte[] data, int dataLength)
        {
            // create or open target file
            if (_fileStream == null)
            {
                _fileStream = new System.IO.MemoryStream();
            }

            _fileStream.Write(data, 0, dataLength);
            return true;
        }

        protected override void CompleteContent()
        {
            recivedData = _fileStream.ToArray();
            // close and save
            _fileStream.Close();
        }
    }
}
