﻿using UnityEngine;
using UnityEngine.UI;
using Wear.ARCompanyCore;
using Wear.Utils.CaptureAndShare;
//using Zenject;
using System.Collections;

#if PLATFORM_ANDROID
using UnityEngine.Android;
#elif PLATFORM_IOS
using UnityEngine.iOS;
#endif

public class CaptureAndSharePanel : MonoBehaviour// WindowController
{
    //public override bool Top => true;

    [SerializeField] private CaptureControl _captureControl;
    [SerializeField] private GameObject contentPrefab;
    [SerializeField] private Text saveText;
    [SerializeField] private Button BackButton;
    [SerializeField] private GameObject _bottomPanel;
    [SerializeField] private Transform parentTrf;

    //[Inject] private ExperiencesManager _experiencesManager;

    private string contentPath;
    private float animDelay = 0f;
    private Animator saveTextAnim;
    private CaptureAndShareUIContentLogic logic;

    void Awake() {
        _captureControl.OnScreenshotCaptured += ScreenshotCallBack;
        _captureControl.OnVideoEndCapture += VideoCallBack;
        CaptureCore.OnVideoCapturingEnd += VideoSaveCallback;
        logic = contentPrefab.GetComponent<CaptureAndShareUIContentLogic>();
        saveTextAnim = saveText.GetComponent<Animator>();

        var scaler = GetComponentsInParent<CanvasScaler>();
    }

    void Start() {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Microphone)) {
            Permission.RequestUserPermission(Permission.Microphone);
        }
#elif PLATFORM_IOS
        StartCoroutine(MicrophoneInit());
#endif
    }

    IEnumerator MicrophoneInit() {
#if UNITY_IOS
            yield return Application.RequestUserAuthorization(UserAuthorization.Microphone); //OR DOWN IN BLOCK
            if (Application.HasUserAuthorization(UserAuthorization.Microphone)) {
                Debug.Log("Microphone found");
            } else {
                Debug.Log("Microphone not found");
            }
#endif
        yield return new WaitForEndOfFrame();
    }

    public float AnimDelay {
        get {
            animDelay = (logic.TimeForOutAnim / 2f) + logic.TimeForWaitAnim + logic.TimeToCornerAnim;
            return animDelay;
        }
        set { animDelay = value; }
    }

    //protected override void Show(params object[] _params)
    //{
    //    _captureControl.OnScreenshotCaptured += ScreenshotCallBack;
    //    _captureControl.OnVideoEndCapture += VideoCallBack;
    //    CaptureCore.OnVideoCapturingEnd += VideoSaveCallback;
    //    logic = contentPrefab.GetComponent<CaptureAndShareUIContentLogic>();
    //    saveTextAnim = saveText.GetComponent<Animator>();
    //    base.Show(_params);
    //}

    //protected override void Closed()
    //{
    //    _captureControl.OnScreenshotCaptured -= ScreenshotCallBack;
    //    _captureControl.OnVideoEndCapture -= VideoCallBack;
    //    CaptureCore.OnVideoCapturingEnd -= VideoSaveCallback;
    //    base.Closed();
    //}

    public void Hide() {
        var cg = gameObject.AddComponent<CanvasGroup>();
        cg.alpha = 0;
        cg.blocksRaycasts = false;
    }

    public void Show() {
        var cg = gameObject.GetComponent<CanvasGroup>();
        if (cg == null) {
            return;
        }

        Destroy(cg);
    }

    public void ShowHideShareButton(bool value) {
        _bottomPanel.SetActive(value);
    }

    public void Share() {
        _captureControl.MediaShare();
    }

    private void ScreenshotCallBack(string path) {
        _captureControl.MediaSave();
        saveText.text = "Image saved";
        saveTextAnim.PlayInFixedTime("SaveAnim");
        GameObject newContent = Instantiate(contentPrefab, parentTrf/*contentPrefab.transform.parent*/);
        newContent.GetComponent<CaptureAndShareUIContentLogic>().Init(false, path, _captureControl.GetScreenshotTexture2D());
    }

    private void VideoSaveCallback() {
        _captureControl.MediaSave();
        saveText.text = "Video saved";
        saveTextAnim.PlayInFixedTime("SaveAnim");

        GameObject newContent = Instantiate(contentPrefab, parentTrf/*contentPrefab.transform.parent*/);
        newContent.transform.SetSiblingIndex(5);
        logic = newContent.GetComponent<CaptureAndShareUIContentLogic>();
        logic.Init(true, _captureControl.GetTempVideoPath(), null);
    }

    private void VideoCallBack(string path) { }

    public void Screenshot() {
        _captureControl.MakeScreenshot();
    }

    public void StartRecord() {
        _captureControl.StartCaptureVideo();
    }

    public void StopRecord() {
        _captureControl.StopCaptureVideo();
    }

    //public void OnBackButtonClick()
    //{
    //    _experiencesManager.DeactivateAndDirty();
    //    Close();
    //}
}

