﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Wear.ARCompanyCore {
    public class QuestionnairePanel : MonoBehaviour {

        [Header("Name panel")]
        [SerializeField] private TMP_InputField nameText;
        [SerializeField] private TMP_InputField numberText;
        [SerializeField] private TMP_Dropdown countryCode;
        [SerializeField] private Button sendValidationBtn;
        [Header("Validation panel")]
        [SerializeField] private TMP_Text numberLayout;
        [SerializeField] private List<TMP_InputField> codeInputs;
        [SerializeField] private TMP_Text timeTimer;
        [SerializeField] private Button confirmCodeBtn;

        void Start() {

        }

        void Update() {

        }
    }
}
