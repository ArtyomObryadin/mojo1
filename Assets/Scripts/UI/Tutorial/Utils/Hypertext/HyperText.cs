﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Utils;

namespace HyperT
{
    [AddComponentMenu("UI/HyperText", 12), Serializable, RequireComponent(typeof(RectTransform), typeof(CanvasRenderer))]
    public class HyperText : HyperTextBase
    {
        private Color _hyperlinkColor = HexColorUtils.HexToColor("4B30D2");
        private Color _emailColor = HexColorUtils.HexToColor("4B30D2");//Color.red;
        private Color _tagColor = HexColorUtils.HexToColor("4B30D2");
        private Color _phoneColor = HexColorUtils.HexToColor("4B30D2");

        const string RegexURL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";
        const string RegexHashtag = "[#＃][Ａ-Ｚａ-ｚA-Za-z]+";
        const string RegexMail = "([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)";
        const string RegexPhone = "(\\d{3})(\\d{3})(\\d{6})";

        readonly Dictionary<string, Entry> _entryTable = new Dictionary<string, Entry>();

        struct Entry
        {
            public string RegexPattern;
            public Color Color;
            public Action<string> OnClick;

            public Entry(string regexPattern, Color color, Action<string> onClick)
            {
                RegexPattern = regexPattern;
                Color = color;
                OnClick = onClick;
            }
        }

        void Start()
        {
            SetClickableByRegex(RegexURL, _hyperlinkColor, pr =>
            {
                OpenUrl(pr);
                Debug.Log(pr);
            });
            SetClickableByRegex(RegexHashtag, _tagColor, pr => {
                OpenHashtag(pr);
                Debug.Log(pr);
            });
            SetClickableByRegex(RegexMail, _emailColor, pr => {
                SendMail(pr);
                Debug.Log(pr);
            });
            SetClickableByRegex(RegexPhone, _phoneColor, pr => {
                OpenUrl("tel://+" + pr);
                Debug.Log(pr);
            });
        }

        public void SetClickableByRegex(string regexPattern, Action<string> onClick)
        {
            SetClickableByRegex(regexPattern, color, onClick);
        }

        public void SetClickableByRegex(string regexPattern, Color color, Action<string> onClick)
        {
            if (string.IsNullOrEmpty(regexPattern) || onClick == null)
            {
                return;
            }

            _entryTable[regexPattern] = new Entry(regexPattern, color, onClick);
        }

        public override void RemoveClickable()
        {
            base.RemoveClickable();
            _entryTable.Clear();
        }

        protected override void RegisterClickable()
        {
            foreach (var entry in _entryTable.Values)
            {
                foreach (Match match in Regex.Matches(text, entry.RegexPattern))
                {
                    RegisterClickable(match.Index, match.Value.Length, entry.Color, entry.OnClick);
                }
            }
        }

        private void OpenHashtag(string url)
        {
            //
        }

        private void OpenUrl(string url)
        {
            Application.OpenURL(url);
        }

        private void SendMail(string mail)
        {
            //email Id to send the mail to
            string email = mail;
            //subject of the mail
            string subject = MyEscapeURL("FEEDBACK/SUGGESTION");
            //body of the mail which consists of Device Model and its Operating System
            string body = MyEscapeURL("Please Enter your message here\n\n\n\n" +
                                      "________" +
                                      "\n\nPlease Do Not Modify This\n\n" +
                                      "Model: " + SystemInfo.deviceModel + "\n\n" +
                                      "OS: " + SystemInfo.operatingSystem + "\n\n" +
                                      "________");
            //Open the Default Mail App
            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
        }

        private string MyEscapeURL(string url)
        {
            return WWW.EscapeURL(url).Replace("+", "%20");
        }
    }
}
