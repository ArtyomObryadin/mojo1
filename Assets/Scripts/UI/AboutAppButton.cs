﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutAppButton : MonoBehaviour
{
    public GameObject PrivacyPolicyButton;
    public void buttonOn(Single value)
    {
        if(value<0.05)
        {
            PrivacyPolicyButton.SetActive(true);
        }
        else
        {
            PrivacyPolicyButton.SetActive(false);
        }
    }
    public void OpenMarkers()
    {
        Application.OpenURL("https://drive.google.com/drive/u/2/folders/1lvGBAFVLogiGlcL9fCgQxM0WUox5nxuf");
    }
}
