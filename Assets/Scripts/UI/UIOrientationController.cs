﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wear.ARCompanyCore.Managers;
using Wikitude;

namespace Wear.ARCompanyCore {
    public class UIOrientationController : MonoBehaviour {

        [SerializeField] private GameObject frameImageObj;
        [SerializeField] private Button infoBtn;
        [SerializeField] private Button flashBtn;
        [SerializeField] private Image flashImg;
        [SerializeField] private CameraSettingsController arCameraController;
        [SerializeField] private GameObject questionnairePnl;
        [SerializeField] private GameObject screenSharePnl;

        //public Button InfoBtn { get => infoBtn; set => infoBtn = value; }

        private void Awake() {
            if (arCameraController != null) {
                //flashBtn.interactable = arCameraController.Camera.IsFlashModeSupported(Wikitude.CaptureFlashMode.On);
            }   
        }

        void Start() { }

        private void OnEnable() {
            infoBtn.onClick.AddListener(Manager.UIManager.ShowHideTutorial);
            flashBtn.onClick.AddListener(FlashBtnChange);

            FlashBtnChange();
        }

        private void OnDisable() {
            infoBtn.onClick.RemoveListener(Manager.UIManager.ShowHideTutorial);
            flashBtn.onClick.RemoveListener(FlashBtnChange);
        }

        void Update() { }

        public void ScanPanelShowHide(bool status) {
            frameImageObj.SetActive(status);
        }

        private void FlashBtnChange() {
            flashImg.color = Manager.UIManager.IsFlashOn ? new Vector4(0.9529f, 0.5882f, 0.4862f, 1f) : new Vector4(0.8274f, 0.8274f, 0.8274f, 1f);
        }
        [SerializeField] public DeviceCamera Camera;
        public void FlashBtnNull()
        {
            Camera.FlashMode = (CaptureFlashMode)0;
            flashImg.color = new Vector4(0.8274f, 0.8274f, 0.8274f, 1f);
        }
    }
}
