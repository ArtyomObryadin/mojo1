﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using Wear.ARCompanyCore.Managers;
using Wear.ARCompanyCore.Utils;
using UniRx.Async;

namespace Wear.ARCompanyCore {

    public enum ContentType {
        Video,
        Effect
    }

    [Serializable]
    public class User {
        public string id;
        public string email;
        public string password_digest;
        public DateTime created_at;
        public DateTime updated_at;
    }    

    [Serializable]
    public class EffectData : ICacheable {
        public string id;
        public string name;
        public string unity_descriptor;
        public string assetbundle_url_android;
        public string assetbundle_url_ios;
        public string created_at;
        public string updated_at;

        CacheInfo _cacheInfo;

        public string CacheId { get => unity_descriptor; }
        public CacheInfo CacheInfo {
            get {
                if (_cacheInfo != null) {
                    return _cacheInfo;
                }
                else {
                    return _cacheInfo = new CacheInfo() {
                        Name = CacheId,
                        LastModifiedData = updated_at,
                        CachePath = Manager.CacheManager.cacheConfig.EffectsCachePath
                    };
                };
            }

            set { _cacheInfo = value; }
        }
    }

    [Serializable]
    public class VideoData : ICacheable {
        public string orientation;
        public string id;
        public string name;
        public string video_url;
        public bool loop;
        public VideoTimeline[] timeline;
        public string created_at;
        public string updated_at;

        CacheInfo _cacheInfo;

        public string CacheId { get => name; }
        public CacheInfo CacheInfo {
            get {
                if (_cacheInfo != null) {
                    return _cacheInfo;
                }
                else {
                    return _cacheInfo = new CacheInfo() {
                        Name = CacheId + Path.GetExtension(video_url),
                        LastModifiedData = updated_at,
                        CachePath = Manager.CacheManager.cacheConfig.VideoCachePath
                    };
                };
            }

            set { _cacheInfo = value; }
        }

        public string GetContentPath() {
            return CacheManager.BaseCachePath + CacheInfo.CachePath + CacheInfo.Name;
        }
    }

    public class Effect {
        public string effectDescription;
        public float timeline;
        public float endtimeline;
        public bool isPersistent;
        public bool isFullScreen;
        [NonSerialized]
        public string effectPath;

        public Effect(VideoTimeline timeLine) {
            effectDescription = timeLine.effect;
            timeline = (float)TimeSpan.Parse(timeLine.timecode).TotalMilliseconds;
            endtimeline = (float)TimeSpan.Parse(timeLine.endtimecode).TotalMilliseconds;
            isPersistent = timeLine.persistent;
            isFullScreen = timeLine.fullscreen;
            //Debug.Log(timeline);
            effectPath = CacheManager.GetEffectPathByName(timeLine.effect);
        }
    }

    [Serializable]
    public class VideoTimeline {
        public string timecode;  //(m:ss:zzz)
        public string endtimecode;  //(m:ss:zzz)
        public string effect;    //(Effect.unity_descriptor ref)
        public bool persistent;
        public bool fullscreen;
    }

    [Serializable]
    public class SerializableVector3 {
        public float x;
        public float y;
        public float z;

        public SerializableVector3(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static implicit operator Vector3(SerializableVector3 rValue) {
            return new Vector3(rValue.x, rValue.y, rValue.z);
        }

        public static implicit operator SerializableVector3(Vector3 rValue)
        {
            return new SerializableVector3(rValue.x, rValue.y, rValue.z);
        }

        public static implicit operator SerializableVector3(Vector2 rValue)
        {
            return new SerializableVector3(rValue.x, rValue.y, 0);
        }

        public override string ToString()
        {
            return $"({x},{y},{z})";
        }
    }
}
