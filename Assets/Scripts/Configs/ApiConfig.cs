﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wear.ARCompanyCore {
    [CreateAssetMenu(fileName = "ApiConfigSO", menuName = "Wear/Core/Configs/Api Config")]
    public class ApiConfig : ScriptableObject {

        public string ConfigLink;

        [SerializeField]
        string BaseEndpoint;
        [SerializeField]
        string GetVideos;
        [SerializeField]
        string GetEffects;
        [SerializeField]
        string SiteUrl;
        //[SerializeField]
        string Authorizationlink;
        //[SerializeField]
        string RegistrationRequest;

        public string BaseEndpointLink {
            get => BaseEndpoint;
            set => BaseEndpoint = value;
        }

        public string AuthorizationLink {
            get => BaseEndpoint + Authorizationlink;
            set => Authorizationlink = value;
        }

        public string GetRegistrationRequest {
            get => BaseEndpoint + RegistrationRequest;
            set => RegistrationRequest = value;
        }

        public string GetVideosLink {
            get => BaseEndpoint + GetVideos;
            set => GetVideos = value;
        }

        public string GetEffectsLink {
            get => BaseEndpoint + GetEffects;
            set => GetEffects = value;
        }

        public string GetSiteUrl {
            get => SiteUrl;
        }
    }
}
