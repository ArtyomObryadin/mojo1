﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Wear.ARCompanyCore {
    [CustomEditor(typeof(ApiConfig))]
    public class CreateJsonFromScriptableObject : Editor {

        private static GUILayoutOption[] glo1;

        void OnEnable() {
            glo1 = new GUILayoutOption[2] { GUILayout.Height(25), GUILayout.Width(300) };
        }

        public override void OnInspectorGUI() {
            base.DrawDefaultInspector();
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Serialize", glo1)) {
                Debug.Log(Selection.activeObject);
                string json = "";
                if (Selection.activeObject is ApiConfig) {
                    json = JsonUtility.ToJson((Selection.activeObject as ApiConfig));
                }

                Debug.Log(json);
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(5);
        }

    }
}

