﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using MiniJSON;
using UniRx.Async;

namespace Wear.ARCompanyCore.Managers {

    public class ContentManager : MonoBehaviour {

        [SerializeField]
        private Transform parentScreenObj;
        [SerializeField]
        private bool isContentInside;
        [SerializeField]
        private bool isLostMarkerDelay = false;

        private List<VideoTimeline> timelines;
        private VideoContentController videoContr;
        private string currentMarkerName = "";
        private bool isFirstStart = true;
        private bool isContentLoaded = false;
        private bool isMarkerLost = false;
        
        public bool IsLoadAllContent { get; set; }
        public List<VideoData> VideoDataList { get; set; }
        public VideoInfo VideosContentInfo { get; set; }
        public EffectInfo EffectsContentInfo { get; set; }
        public bool IsContentInside { get => isContentInside; set => isContentInside = value; }
        public bool IsVideoContentPlay { get; set; }
        public double CurrentVideoStopTime { get; set; }
        public double CurrentVideoClipLength { get; set; }
        public bool IsFrontCameraOn { get; set; }
        public Transform ParentScreenObj { get => parentScreenObj; set => parentScreenObj = value; }

        void Start() {
            Init();
        }

        private async void Init() {
            IsLoadAllContent = false;
            await CheckContent();
        }

        private async UniTask CheckContent() {
            await UniTask.WaitUntil(() => Manager.ApiManager.ConfigStatus == LoadConfigStatus.Loaded);
            EffectsContentInfo = new EffectInfo();
            VideosContentInfo = new VideoInfo();
            return;
        }

        #region NO_Cloud Methods

        public async void TargetRecognized(GameObject targetContent, string name) {
            StopCoroutine("LostTargetDelay");
            videoContr = targetContent.GetComponentInChildren<VideoContentController>();

            isContentLoaded = false;
            isMarkerLost = false;
            await UniTask.WaitUntil(() => VideosContentInfo.CurrentContent.FirstOrDefault(x => x.name == name).name == name);

            if (isFirstStart) {
                isFirstStart = false;
                videoContr.ClearVideoTexture();
            }

            if (!string.IsNullOrEmpty(currentMarkerName)) {
                if (currentMarkerName != name) {
                    videoContr.ClearVideoTexture();
                    CurrentVideoStopTime = 0d;
                }
            }

            currentMarkerName = name;

            Manager.UIManager.ScanPanelControll(true);

            if (!Manager.UIManager.GetLoadingPanelStatus()) {
                if (Application.internetReachability == NetworkReachability.NotReachable) {
                    Debug.Log("---------NO INTERNET---------");
                    Manager.UIManager.ShowNoInternetPopUp(); 
                }

                await UniTask.WaitUntil(() => Application.internetReachability != NetworkReachability.NotReachable);
                Manager.UIManager.ShowNoInternetPopUp(false);
                Manager.UIManager.LoadingPanelController(true);

                await VideosContentInfo.GetConcreteContentPath(name);

                isContentLoaded = true;

                Manager.UIManager.LoadingPanelController(false);
                if (!isMarkerLost) {
                    videoContr.Init(VideosContentInfo.CachedContent.FirstOrDefault(n => n.name == name));
                }

                if (CurrentVideoStopTime == 0d) {
                    Debug.Log("Video IN THE BEGIN");
                }
            }     
        }
        IEnumerable LostTargetDelay()
        {
            yield return new WaitForSeconds(3);
            if (!IsVideoContentPlay)
            {
                CurrentVideoStopTime = 0d;
                Debug.Log("after 3000 ms is over");

                var canvasObj = parentScreenObj.GetComponentsInChildren<UnityEngine.UI.Image>(true);

                for (int i = 0; i < canvasObj?.Length; i++)
                {
                    Destroy(canvasObj[i].gameObject);
                }
            }

        }
        public async void TargetLost(GameObject targetContent, string name) {
            IsVideoContentPlay = false;
            isMarkerLost = true;

            await UniTask.WaitUntil(() => isContentLoaded);
            Debug.Log("============= isMarkerLost --> " + isMarkerLost);
            if (isMarkerLost) {
                Manager.UIManager.ScanPanelControll(false);
            }
            Debug.Log(CurrentVideoStopTime);
            if (CurrentVideoStopTime != 0d && CurrentVideoClipLength != Math.Round(CurrentVideoStopTime, 2) && isLostMarkerDelay) {
                Debug.Log("before 3000 ms is over");

                var canvasObj = parentScreenObj.GetComponentsInChildren<UnityEngine.UI.Image>(true);

                for (int i = 0; i < canvasObj?.Length; i++)
                {
                    canvasObj[i].gameObject.SetActive(false);
                }

                StartCoroutine("LostTargetDelay");
            }
            else {
                CurrentVideoStopTime = 0d;
                Debug.Log("Must DELETE CANVAS OBJECT");
                var canvasObj = parentScreenObj.GetComponentsInChildren<UnityEngine.UI.Image>(true);

                for (int i = 0; i < canvasObj?.Length; i++) {
                    Destroy(canvasObj[i].gameObject);
                }
            }
        }
        #endregion

        public float TimeLineConvert(string timeline) {
            float result = 0f;
            string[] tempTime = timeline.Split(':');
            List<int> tempIntTime = new List<int>();

            foreach (var elem in tempTime) {
                tempIntTime.Add(int.Parse(elem));
            }

            if (tempIntTime.Count == 3) {
                result = tempIntTime[0] * 60 + tempIntTime[1] + (float)tempIntTime[2] / 1000;
            }

            return result;
        }
    }
}
