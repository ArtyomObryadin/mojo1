﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Wear.ARCompanyCore.Managers
{
    public class UIManager : MonoBehaviour
    {

        [Header("Orientation Objects")]
        [SerializeField] private UIOrientationController portraitOrientObj;
        [SerializeField] private UIOrientationController landscapeOrientObj;
        [Header("Panels")]
        [SerializeField] private GameObject tutorialPanel;
        [SerializeField] private GameObject aboutPanel;
        [SerializeField] private GameObject downloadPanel;
        [SerializeField] private GameObject loadingPanel;
        [SerializeField] private GameObject downloadingPanel;
        [Header("PopUps")]
        [SerializeField] private GameObject noInternetPopUp;
        [SerializeField] private GameObject quitPopUp;
        [SerializeField] private Button quitBtn;

        DeviceOrientation currentDeviceOrientation;
        bool isTutorialOpen = false;

        public bool IsFlashOn { get; set; }

        void Start()
        {
            portraitOrientObj.gameObject.SetActive(true);

            if (!PlayerPrefs.HasKey("TutorialShowed") || PlayerPrefs.GetInt("TutorialShowed") == 0)
            {
                ShowHideTutorial();
                PlayerPrefs.SetInt("TutorialShowed", 1);
                currentDeviceOrientation = DeviceOrientation.Portrait;
            }
            else
            {
                currentDeviceOrientation = Input.deviceOrientation;
            }
        }

        private void OnEnable()
        {
            quitBtn.onClick.AddListener(Application.Quit);
        }

        private void OnDisable()
        {
            quitBtn.onClick.RemoveAllListeners();
        }

        void Update()
        {
            if (currentDeviceOrientation != Input.deviceOrientation && !isTutorialOpen)
            {
                if (Input.deviceOrientation == DeviceOrientation.Portrait || Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
                {
                    portraitOrientObj.gameObject.SetActive(true);
                    landscapeOrientObj.gameObject.SetActive(false);
                    Screen.orientation = (ScreenOrientation)Input.deviceOrientation;
                }
                else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                {
                    portraitOrientObj.gameObject.SetActive(false);
                    landscapeOrientObj.gameObject.SetActive(true);
                    Screen.orientation = (ScreenOrientation)Input.deviceOrientation;
                }

                currentDeviceOrientation = Input.deviceOrientation;
            }
#if UNITY_ANDROID || UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Escape) /*&& !popUpPanel.activeSelf && IsNeedGoBack*/)
            {
                ShowQuitPopUp();
                //IsNeedGoBack = false;
                Debug.Log("Escape Btn is ckicked");
                //GoBack();
            }
#endif
        }

        public void ShowHideTutorial()
        {
            bool status = tutorialPanel.activeSelf;
            tutorialPanel.SetActive(!status);

            isTutorialOpen = !status;
            Screen.orientation = ScreenOrientation.Portrait;

            if (!status)
            {
                landscapeOrientObj.gameObject.SetActive(false);
                portraitOrientObj.gameObject.SetActive(true);
            }
        }

        void ScreenOrientationControll()
        {
            bool isPortraintorient = Screen.orientation == ScreenOrientation.Portrait;
            portraitOrientObj.gameObject.SetActive(isPortraintorient);
            landscapeOrientObj.gameObject.SetActive(!isPortraintorient);
        }

        public void LoadingPanelController(bool status)
        {
            loadingPanel.SetActive(status);
        }

        public bool GetLoadingPanelStatus()
        {
            return loadingPanel.activeSelf;
        }

        public void ScanPanelControll(bool status)
        {
            portraitOrientObj.ScanPanelShowHide(!status);
            landscapeOrientObj.ScanPanelShowHide(!status);
        }

        public void ShowNoInternetPopUp(bool status = true)
        {
            noInternetPopUp.SetActive(status);
        }

        public void ShowQuitPopUp()
        {
            quitPopUp.SetActive(true);
        }

        public void DownLoadPanelController(bool isActive)
        {
            downloadingPanel.SetActive(isActive);
        }
    }
}
