﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UnityEngine;
using UnityEngine.Networking;

using UniRx;
using UniRx.Async;

using Wear.ARCompanyCore.Utils;
using MiniJSON;

namespace Wear.ARCompanyCore.Managers {

    public enum LoadConfigStatus {
        LoadStart,
        Loaded
    }

    public class ApiManager : MonoBehaviour {

        public static ApiConfig config;
        [SerializeField]
        ApiConfig _apiConfig;

        public LoadConfigStatus ConfigStatus { get; set; }

        void Start() { Configurate(); }

        private void Awake() {
            //Configurate();
        }

        async void Configurate() {
            config = _apiConfig;
            ConfigStatus = LoadConfigStatus.LoadStart;

            if (Application.internetReachability == NetworkReachability.NotReachable) {
                Debug.Log("-----NO INTERNET-----");
                Manager.UIManager.ShowNoInternetPopUp(); // solid
            }

            await UniTask.WaitUntil(() => Application.internetReachability != NetworkReachability.NotReachable);
            Manager.UIManager.ShowNoInternetPopUp(false);

            UnityWebRequest op = await UnityWebRequest.Get(_apiConfig.ConfigLink).SendWebRequest();

            Debug.LogFormat("<color=#ff0000ff> Request After Sending </color>");

            UnityWebRequest www = op;

            if (www.isDone) {
                JsonUtility.FromJsonOverwrite(www.downloadHandler.text, _apiConfig);
                Debug.Log(_apiConfig.BaseEndpointLink);
                Debug.Log("RequestComplete");
                ConfigStatus = LoadConfigStatus.Loaded;
            }
            else {
                Debug.Log(new WebRequestStatus(www).RawErrorMessage);
            }

            //Debug.LogFormat("<color=#00ff00ff> AMAZONETEST " + _apiConfig.GetRegistrationRequest + " </color>");
            //await UniTask.WaitUntil(() => !string.IsNullOrEmpty(_apiConfig.GetRegistrationRequest));

            //authorizeURL = _apiConfig.AuthorizationLink;
            //await Authorize(authorizeURL);
        }

        public static async UniTask<string> GetVideosList (Action<WebRequestStatus> requestStatus = null) {
            //if (GetNewAuthorizeToken() || IPManager.CheckIPv4()) {
            //    await UniTask.WaitUntil(() => !string.IsNullOrEmpty(authorizeURL));
            //    await Authorize(authorizeURL);
            //}

            Debug.Log("GetVideosList LINK ---> " + (config.GetVideosLink));

            var www = await GetResponse(UnityWebRequest.Get(config.GetVideosLink));

            if (www.IsSuccessfull()) {
                requestStatus?.Invoke(new WebRequestStatus(www, "Video Array file was loaded sucsessfull!!"));
                return www.downloadHandler.text;
            }
            else {
                requestStatus?.Invoke(new WebRequestStatus(www, www.error));
                Debug.Log(www.downloadHandler.text);

                return "";
            }
        }

        public static async UniTask<string> GetEffectsList(Action<WebRequestStatus> requestStatus = null) {
            //if (GetNewAuthorizeToken() || IPManager.CheckIPv4()) {
            //    await UniTask.WaitUntil(() => !string.IsNullOrEmpty(authorizeURL));
            //    await Authorize(authorizeURL);
            //}

            Debug.Log("GetEffectsList LINK ---> " + (config.GetEffectsLink));

            var www = await GetResponse(UnityWebRequest.Get(config.GetEffectsLink));

            if (www.IsSuccessfull()) {
                requestStatus?.Invoke(new WebRequestStatus(www, "Effect Array file was loaded sucsessfull!!"));
                return www.downloadHandler.text;
            }
            else {
                requestStatus?.Invoke(new WebRequestStatus(www, www.error));
                Debug.Log(www.downloadHandler.text);

                return "";
            }
        }

        public static async UniTask GetContentFromServer(string url, CacheInfo cacheInfo, Action<WebRequestStatus> requestStatus = null) {
            //if (GetNewAuthorizeToken() || IPManager.CheckIPv4()) {
            //    await UniTask.WaitUntil(() => !string.IsNullOrEmpty(authorizeURL));
            //    await Authorize(authorizeURL);
            //}

            string contentUrl = url;
            Debug.Log("DownloadContent by path " + contentUrl);

            UnityWebRequest www = UnityWebRequest.Get(contentUrl);
            string filename = cacheInfo.Name;

            var filepath = Path.Combine(CacheManager.BaseCachePath + cacheInfo.CachePath, filename);

            if (!Directory.Exists(CacheManager.BaseCachePath + cacheInfo.CachePath)) {
                Directory.CreateDirectory(CacheManager.BaseCachePath + cacheInfo.CachePath);
            }

            www.downloadHandler = new DownloadHandlerFile(filepath);

            var AsyncOperation = await GetResponse(www);
            if (AsyncOperation.IsSuccessfull()) {
                await UniTask.WaitUntil(() => AsyncOperation.isDone && AsyncOperation.downloadProgress == 1);
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, "Content was Downloaded Successfuly"));
            }
            else {
                Debug.LogError("Content wasn't downloaded!");
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, AsyncOperation.error));
            }
        }

        public static async UniTask<UnityWebRequest> GetResponse(UnityWebRequest req) {
            // req.SetRequestHeader("Content-Type", "application/json");
            //req.SetRequestHeader("Authorization", "Bearer " + barier);
            //Debug.LogFormat("<color=#ff0000ff> Request Before Sending " + req.url + "</color>");

            var op = await req.SendWebRequest();

            Debug.LogFormat("<color=#ff0000ff> Request After Sending </color>");
            UnityWebRequest asyncwww = op;
            return asyncwww;
        }


        public static async UniTask<UnityWebRequest> GetResponse(UnityWebRequest req, Dictionary<string, string> headers) {
            // req.SetRequestHeader("Content-Type", "application/json");
            foreach (KeyValuePair<string, string> header in headers) { 
                req.SetRequestHeader(header.Key, header.Value);
            }

            var op = await req.SendWebRequest();

            Debug.LogFormat("<color=#ff0000ff> Request After Sending </color>");
            UnityWebRequest asyncwww = op;
            return asyncwww;
        }

        public static async UniTask GetEffectsFromServer(string url, CacheInfo cacheInfo, Action<WebRequestStatus> requestStatus = null) {
            //if (GetNewAuthorizeToken() || IPManager.CheckIPv4()) {
            //    await UniTask.WaitUntil(() => !string.IsNullOrEmpty(authorizeURL));
            //    await Authorize(authorizeURL);
            //}

            string bundleUrl = url;
            var filepath = Path.Combine(Application.persistentDataPath + cacheInfo.CachePath, cacheInfo.Name);

            Debug.Log("DownloadBundle by path " + bundleUrl);

            UnityWebRequest www = UnityWebRequest.Get(bundleUrl);
            www.downloadHandler = new DownloadHandlerFile(filepath);

            var AsyncOperation = await GetResponse(www);
            if (AsyncOperation.IsSuccessfull()) {
                await UniTask.WaitUntil(() => AsyncOperation.isDone && AsyncOperation.downloadProgress == 1);
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, "Bundle Downloaded"));
            }
            else {
                Debug.LogError("Effect is Not downloaded!");
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, AsyncOperation.error));
            }
        }

        public static async UniTask GetVoiseContentFromServer(string url, CacheInfo cacheInfo, Action<WebRequestStatus> requestStatus = null) {
            //if (GetNewAuthorizeToken() || IPManager.CheckIPv4()) {
            //    await UniTask.WaitUntil(() => !string.IsNullOrEmpty(authorizeURL));
            //    await Authorize(authorizeURL);
            //}

            string videoUrl = config.BaseEndpointLink + "/" + url;
            Debug.Log("Download Video file by path " + videoUrl);

            UnityWebRequest www = UnityWebRequest.Get(videoUrl);
            string filename = cacheInfo.Name;

            var filepath = Path.Combine(Application.persistentDataPath + cacheInfo.CachePath, filename);
            www.downloadHandler = new DownloadHandlerFile(filepath);

            var AsyncOperation = await GetResponse(www);
            if (AsyncOperation.IsSuccessfull()) {
                await UniTask.WaitUntil(() => AsyncOperation.isDone && AsyncOperation.downloadProgress == 1);
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, "Video file was Downloaded"));
            }
            else {
                Debug.LogError("Video file was Not downloaded!");
                requestStatus?.Invoke(new WebRequestStatus(AsyncOperation, AsyncOperation.error));
            }
        }
    }
}
