﻿using UniRx.Async;

namespace Wear.ARCompanyCore {
    public interface IContent {
        ContentType contentType { get; set; }
        //string ContentPath { get; set; }
        UniTask Init();
        UniTask GetAllContentList();
        UniTask<bool> GetContentPath(string name);
    }
}
