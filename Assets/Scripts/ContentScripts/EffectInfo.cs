﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UniRx.Async;

using Wear.ARCompanyCore.Utils;
using Wear.ARCompanyCore.Managers;

namespace Wear.ARCompanyCore {
    public class EffectInfo : IContent, ICacheable {

        List<EffectData> currentContent;
        CacheInfo _cacheInfo;

        public List<EffectData> CurrentContent { get => currentContent; set => currentContent = value; }
        public List<EffectData> CachedContent { get; set; }
        public ContentType contentType { get; set; }
        //public string ContentPath { get; set; }
        public string CacheId { get => Manager.CacheManager.cacheConfig.EffectsInfoName; }
        public CacheInfo CacheInfo {
            get {
                if (_cacheInfo != null) {
                    return _cacheInfo;
                }
                else {
                    return _cacheInfo = new CacheInfo() {
                        Name = CacheId,
                        LastModifiedData = DateTime.Now.ToString(),
                        CachePath = ""
                    };
                };
            }

            set { _cacheInfo = value; }
        }

        public EffectInfo() {
            Init();
        }

        public async UniTask Init()
        {
            contentType = ContentType.Effect;
            await GetAllContentList();

            await UniTask.WaitUntil(() => CurrentContent != null && CurrentContent.Count > 0);
            //ContentPath = CacheManager.GetInfoFilePath(contentType);

            CachedContent = CacheManager.GetObjectByType<List<EffectData>>(CacheInfo);

            RevalidateVideoContent(Manager.ContentManager.IsLoadAllContent);
        }

        public async UniTask GetAllContentList()
        {
            string json = await ApiManager.GetEffectsList();
            currentContent = new List<EffectData>();

            MainUtils.JsonParse(json, ref currentContent);
        }

        private async void RevalidateVideoContent(bool isLoadAllContent) {    //?????
            List<string> needReload = new List<string>();
            List<bool> isContentAddedList = new List<bool>();

            if (CachedContent != null) {
                if (!isLoadAllContent) {
                    foreach (var elemCach in CachedContent) {
                        foreach (var elemBack in CurrentContent) {
                            if (elemCach.id == elemBack.id) {
                                if (elemCach.updated_at != elemBack.updated_at) {
                                    needReload.Add(elemCach.unity_descriptor);
                                }

                                break;
                            }
                        }
                    }
                }
                else {
                    foreach (var elemBack in CurrentContent) {
                        bool isNeedReload = true;
                        foreach (var elemCach in CachedContent) {
                            if (elemCach.id == elemBack.id) {
                                if (elemCach.updated_at == elemBack.updated_at) {
                                    isNeedReload = false;
                                }

                                break;
                            }
                        }

                        if (isNeedReload) {
                            needReload.Add(elemBack.unity_descriptor);
                        }
                    }
                }
            }
            else {
                if (isLoadAllContent && CurrentContent != null) {
                    foreach (var elem in CurrentContent) {
                        needReload.Add(elem.name);
                    }
                }
            }

            foreach (var elem in needReload) { //Need Loader
                isContentAddedList.Add(await GetContentPath(elem));                 //?????
            }

            if (isContentAddedList.Any(n => n)) {
                CacheManager.CacheObject(CachedContent, CacheInfo);
            }
        }

        public async UniTask GetEffectsAndCache(List<string> names) {
            List<string> needLoad = new List<string>();
            List<bool> isContentAddedList = new List<bool>();
            bool isInCache = false;

            for (int k = 0; k < names?.Count; k++) {
                isInCache = false;
                for (int i = 0; i < CachedContent?.Count; i++) {
                    if (CachedContent[i].unity_descriptor == names[k]) {
                        isInCache = true;
                        break;
                    }
                }

                if (!isInCache) {
                    needLoad.Add(names[k]);
                }
            }

            for (int i = 0; i < needLoad.Count; i++) {
                isContentAddedList.Add(await GetContentPath(needLoad[i]));
            }

            if (isContentAddedList.Any(n => n)) {
                CacheManager.CacheObject(CachedContent, CacheInfo);
            }
        }

        //public async void GetEffectAndCache(string effectName) {
        //    List<string> needLoad = new List<string>();
        //    List<bool> isContentAddedList = new List<bool>();
        //    CacheManager.
        //    bool isInCache = false;

        //    isInCache = false;
        //    for (int i = 0; i < CachedContent?.Count; i++) {
        //        if (CachedContent[i].unity_descriptor == effectName) {
        //            break;
        //        }
        //    }

        //    isContentAddedList.Add(await GetContentPath(effectName));

        //    if (isContentAddedList.Any(n => n)) {
        //        CacheManager.CacheObject(CachedContent, CacheInfo);
        //    }
        //}

        public async UniTask<bool> GetContentPath(string name) {
            EffectData currentElem = null;
            bool isContentInCache = false;
            //string contentPath = "";

            await UniTask.WaitUntil(() => CurrentContent?.Count > 0);
            foreach (var elem in CurrentContent) {
                if (name == elem.unity_descriptor) {
                    currentElem = elem;
                   //contentPath = CacheManager.BaseCachePath + currentElem.CacheInfo.CachePath + currentElem.CacheInfo.Name;
                    break;
                }
            }

            //for (int i = 0; i < CachedContent?.Count; i++) {
            //    if (CachedContent[i].id == currentElem.id && CachedContent[i].updated_at == currentElem.updated_at) {
            //        isContentInCache = true;
            //        break;
            //    }
            //}

            if (/*!isContentInCache &&*/ currentElem != null) {
                isContentInCache = await GetConcreteContent(currentElem);
            }

            //return isContentInCache ? contentPath : "";
            return isContentInCache;
        }

        //async UniTask<bool> GetConcreteContent(string name) {
        async UniTask<bool> GetConcreteContent(EffectData curretnElem)
        {
            if (curretnElem != null) {
                CacheManager.CheckFileByInfoAndDelete(CacheInfo);

                bool isError = true;

                string effectUrl = "";
#if UNITY_ANDROID || UNITY_EDITOR
                effectUrl = curretnElem.assetbundle_url_android;
#elif UNITY_IOS
                effectUrl = curretnElem.assetbundle_url_ios;
#endif

                Debug.LogFormat("<color=#ff0000ff> EEEEFFFFFEEEECCCTTT " + effectUrl + " </color>");
                await ApiManager.GetContentFromServer(effectUrl, curretnElem.CacheInfo, (x) => { isError = !x.WWW.IsSuccessfull(); Debug.Log(x.Text); MainUtils.CheckError(x.Text); });

                if (!isError) {
                    if (CachedContent?.Count > 0) {
                        for (int i = 0; i < CachedContent.Count; i++) {
                            if (curretnElem.id == CachedContent[i].id) {
                                CachedContent[i] = curretnElem;
                                return true;
                            }
                        }
                    }

                    CachedContent.Add(curretnElem);
                    //CacheManager.CacheObject(CachedContent, CacheInfo);
                    return true;
                }
            }
            else {
                Debug.LogError("Effect with name " + curretnElem.name + " doesn't exist on server side");
            }

            return false;
        }
    }
}
