using UnityEngine;
using System.Collections;

public class ApplyShader : MonoBehaviour
{
    public int fromShader = 3200;
    private Material[] thisMaterial;
    private string[] shaders;
    void OnEnable()
    {
        Renderer[] meshRenderArray = GetComponentsInChildren<Renderer>(true);

        //UnityEngine.Debug.Log(meshRenderArray.Length);
        int i = 0;
        foreach (Renderer mr in meshRenderArray)
        {
            //UnityEngine.Debug.Log(i++);
            //UnityEngine.Debug.Log(mr.name + " " + mr.sharedMaterials.Length);

            if (mr.sharedMaterials.Length > 1)
            {
                //UnityEngine.Debug.Log("Shader 1 - " + mr.sharedMaterials[0]);
                //UnityEngine.Debug.Log("Shader 2 - " + mr.sharedMaterials[1]);
            }

            float[] rendererType;
            //                message("Found MeshRenderer " + mr.name);
            foreach (Material m in mr.sharedMaterials)
            {

                if (m != null)
                {
                    var shaderName = m.shader.name;
                    //Debug.Log(shaderName);
                    var newShader = Shader.Find(shaderName);
                    if (newShader != null)
                    {
                        //Debug.Log("NOT NULL SHader - " + shaderName);
                        m.shader = newShader;
                        m.renderQueue = fromShader;
                        rendererType = m.GetFloatArray("_Mode");
                        if (rendererType == null)
                        {
                            continue;
                        }
                        else if (m.GetFloat("_Mode") == 1)
                        {
                            m.SetFloat("_Mode", 1);
                            m.renderQueue = 3100;
                        }
                        else if (m.GetFloat("_Mode") == 3)
                        {
                            m.SetFloat("_Mode", 3);
                            m.renderQueue = 3000;
                        }
                        else
                        {
                            m.renderQueue = 2000;
                        }
                    }
                    else
                    {
                        //                    message("unable to refresh shader: " + shaderName + " in material " + m.name);
                        //UnityEngine.Debug.Log("unable to refresh shader: " + shaderName + " in material " + m.name);
                    }
                }
            }
        }
    }


}