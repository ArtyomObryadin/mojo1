using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Text.RegularExpressions;

public class CreateBundles : MonoBehaviour {
    static string buildPath;
    static bool isExternalBuild;
    static List<AssetBundleBuild> buildAssets = new List<AssetBundleBuild>();

    public  static string FormatBundleName(string name) {
        char[] replaceChars = new char[] { ' ', '"', '*', '+', ',', '(', ')', '-', '.' };
        string frmtString = name;
        foreach (var ch in replaceChars) {
            frmtString = frmtString.Replace(ch, '_');
        }
        frmtString = Regex.Replace(frmtString, "_+", "_");
        frmtString = frmtString.ToLower();
        
        return frmtString;
    }

    [MenuItem("Assets/TestRegex")]
    public static void TestRegex() {
        Debug.Log(FormatBundleName("Acer Aspire 3 A315 - 31(NX.GR4EU.005)_b"));
    }

    [MenuItem("Assets/Build AssetBundles Both")]
    public static void BuildAllAssetBundlesBoth() {   
        // Android
        Directory.CreateDirectory(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), "Android"));
        BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/AssetBundles/Android", BuildAssetBundleOptions.None /*ChunkBasedCompression*/, BuildTarget.Android);
        // IOS
        Directory.CreateDirectory(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), "IOS"));
        BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/AssetBundles/IOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }

//  [MenuItem("Assets/Build AssetBundles For IOS")]
    public static void BuildAllAssetBundlesForIOS() {
        Directory.CreateDirectory(Path.Combine(buildPath, "iOS"));
        BuildPipeline.BuildAssetBundles(Path.Combine(buildPath, "iOS"), buildAssets.ToArray(), BuildAssetBundleOptions.None, BuildTarget.iOS);
    }

    [MenuItem("Assets/Build AssetBundles For Android")]
    public static void BuildAllAssetBundlesForAndroid() {
        Directory.CreateDirectory(Path.Combine(Application.streamingAssetsPath, "Android"));
        BuildPipeline.BuildAssetBundles(Path.Combine(Application.streamingAssetsPath, "Android")/*, buildAssets.ToArray()*/, BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Assets/Get AssetBundle Names")]
    public static void GetNames() {
        var names = AssetDatabase.GetAllAssetBundleNames();
        foreach (var name in names) {
            Debug.Log("AssetBundle name is : " + name);
        }
    }

  //  [MenuItem("Assets/BundelManager/RemoveAllNames")]
    public static void RemoveNames() {
        if (!isExternalBuild && EditorUtility.DisplayDialog("WARNING", "Are you realy wont to delete all asset names NiGGa? This operation is not irreversible!", "Yes", "Cancel")) {
            foreach (var abName in AssetDatabase.GetAllAssetBundleNames()) {
                AssetDatabase.RemoveAssetBundleName(abName, true);
            }
        } else if (isExternalBuild) {
            foreach (var abName in AssetDatabase.GetAllAssetBundleNames()) {
                AssetDatabase.RemoveAssetBundleName(abName, true);
            }
        }
    }

    [MenuItem("Assets/BundelManager/Set Asset Bundle From File Name %#n")]
    public static void SetAssetBundlesFromFileNames() {
        if (Selection.assetGUIDs.Length > 0) {
            foreach (Object asset in Selection.objects) {
                string path = AssetDatabase.GetAssetPath(asset);
                AssetImporter assetImporter = AssetImporter.GetAtPath(path);
                assetImporter.assetBundleName = asset.name;
                Debug.Log(Selection.assetGUIDs.Length + " Asset Bundles Assigned");
            }
        } else {
            Debug.Log("No Assets Selected");
        }
    }

    [MenuItem("Assets/BundelManager/Set Asset Bundle Names By Folder")]
    public static void SetAssetbundleNameByFolder() {
        Debug.Log(buildAssets.Count);
        buildAssets.Clear();
        buildAssets.RemoveRange(0, buildAssets.Count);
        foreach (var path in AssetDatabase.GetAllAssetPaths()) {
            if (Path.GetFullPath(path).Replace('\\', '/').Contains(AssetDatabase.GetAssetPath(Selection.objects[0])) && Path.GetFileName(path).Contains("prefab")) {
                Path.GetFileName(path);

                Debug.Log(path);
                buildAssets.Add(new AssetBundleBuild() {
                    assetBundleName = FormatBundleName(Path.GetFileNameWithoutExtension(path)),
                    assetNames = new string[] { path }
                });

                Debug.LogFormat("<color=#ff0000ff>" + buildAssets[0].assetNames[0] + "</color>");
                Debug.Log(Path.GetFileName(path));
            }
        }
    }

    [MenuItem("Assets/BundelManager/BuidAssetbundleByFolder")]
    public static void BuidAssetbundleByFolder() {
        Debug.Log(buildAssets.Count);
        buildAssets.Clear();
        buildAssets.RemoveRange(0, buildAssets.Count);
        foreach (var path in AssetDatabase.GetAllAssetPaths()) {
            if (Path.GetFullPath(path).Replace('\\', '/').Contains(AssetDatabase.GetAssetPath(Selection.objects[0])) && Path.GetFileName(path).Contains("prefab")) {
                Path.GetFileName(path);
                Debug.Log(path);
                buildAssets.Add(new AssetBundleBuild() {
                    assetBundleName = FormatBundleName(Path.GetFileNameWithoutExtension(path)),
                    assetNames = new string[] { path }
                });
            }
        }
        BuildAllAssetBundlesForIOS();
        BuildAllAssetBundlesForAndroid();
    }

    [MenuItem("Assets/BundelManager/BuildTestBundle")]
    static void BuildBundles() {
        string paramsPath = null;
        Debug.Log(Application.dataPath);
        Debug.Log(File.Exists(Path.Combine(Application.dataPath, "params.txt")));
        if (File.Exists(Path.Combine(Application.dataPath, "params.txt"))) {
            using (var reader = new StreamReader(Path.Combine(Application.dataPath, "params.txt"))) {
                var line = reader.ReadLine();
                paramsPath = line;
                paramsPath = paramsPath.Replace('\\', '/');
                buildPath = reader.ReadLine();
                buildPath = buildPath.Replace('\\', '/');
                Debug.Log(paramsPath);
            }
        }
        foreach (var abName in AssetDatabase.GetAllAssetBundleNames()) {
            AssetDatabase.GetAssetPathsFromAssetBundle(abName);
            string tempBundlePath = AssetDatabase.GetAssetPathsFromAssetBundle(abName)[0];
            tempBundlePath = Path.GetFullPath(tempBundlePath).Replace("\\" + Path.GetFileName(tempBundlePath), "");
            tempBundlePath = tempBundlePath.Replace('\\', '/');

            if (tempBundlePath.Contains(paramsPath + "/")) {
                Debug.Log("Build bundle! " + abName);
            } else {
                AssetDatabase.RemoveAssetBundleName(abName, true);
            }
        }

        BuildAllAssetBundlesForAndroid();
        BuildAllAssetBundlesForIOS();
    }
}
