﻿using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;

public class BuildAssetBundleEditorScript : ScriptableWizard //   EditorWindow
{
    private static string uncompressedTexturesText = "";
    private static BuildAssetBundleEditorScript babes;
    private static Dictionary<string, int> uncompressed;
    private static int uncompressedAndroidTex = 0;
    private static bool showPosition;
    private static Vector2 scrollPos;
    private static GUILayoutOption[] glo1, glo2;
    private static bool testBool1 = true;
    private static bool testBool2 = false;

    private GUIStyle style, style1;
    private List<int> testList;
    private int uncompressTexturesExist = 1;

   // [MenuItem("MyTools/MakeAssetBundles %#m")]
    static public void StartRenaming() {
        FindUncompresedTextures();
        //babes = (BuildAssetBundleEditorScript)EditorWindow.GetWindow(typeof(BuildAssetBundleEditorScript));
        babes = DisplayWizard<BuildAssetBundleEditorScript>("Make Bundles", "Next", "Cancel");
        babes.minSize = new Vector2(300, 300);
        babes.maxSize = new Vector2(300, 500);
        babes.Focus();
        babes.Show();
    }

    static bool FindPrefabsWithAssetBundlesNames() {
        return AssetDatabase.GetAllAssetBundleNames().Length != 0;
    }

    static void FindUncompresedTextures() {
        UnCompressedPngFind unCompressedTextureFind = new UnCompressedPngFind();
        uncompressedTexturesText = "";
        unCompressedTextureFind.StartFindMethod();

        while (!UnCompressedPngFind.FindingEnd) { }

        if (UnCompressedPngFind.UnCompressed.Count > 0) {
            uncompressed = UnCompressedPngFind.UnCompressed;
            foreach (var elem in UnCompressedPngFind.UnCompressed) {
                uncompressedTexturesText += elem.Key + " - " + elem.Value + "\n";
            }
        }

        uncompressedAndroidTex = UnCompressedPngFind.UnCompressed["Haven't Android Compression"];
    }

    public static void Bla() {
        EditorUtility.FocusProjectWindow();
    }

    private void OnEnable() {
        style = new GUIStyle();
        style1 = new GUIStyle();
        uncompressed = new Dictionary<string, int>();
        testList = new List<int>();
        style.fontSize = 12;
        style.fontStyle = FontStyle.Bold;
        style.alignment = TextAnchor.MiddleCenter;
        style1.fontSize = 14;
        style.fontStyle = FontStyle.Bold;
        style.alignment = TextAnchor.MiddleCenter;
        glo1 = new GUILayoutOption[1] { GUILayout.Height(25) };
        glo2 = new GUILayoutOption[1] { GUILayout.Height(20) };
    }

    private void OnGUI() {
        GUIContent content = new GUIContent("<color=yellow>" + uncompressedTexturesText + "</color>");
        GUIContent content1 = new GUIContent("<color=white> AssetBundles Build </color>");
        GUILayout.BeginVertical();
        GUILayout.Space(20);
        GUILayout.Box(content, style);
        EditorGUILayout.EndFadeGroup();

        if (GUILayout.Button("Check uncompressed textures", glo1)) {
            FindUncompresedTextures();
            Repaint();
        }

        GUILayout.Space(10);

        if (UnCompressedPngFind.UnCompressed["Haven't iOS Compression"] != 0) {
            if (GUILayout.Button("From Android to iOS")) {
                if (EditorUtility.DisplayDialog("WARNING", "Do you really want to recompress textures? This operation is not irreversible!", "Yes", "Cancel")) {
                    TextureCompressor.FromAndroidToiOSCompression(UnCompressedPngFind.UncompressediOSList);
                    Debug.LogFormat("<color=#00ffffff> Copmressing From Android to iOS was occured </color>");
                    FindUncompresedTextures();
                    Repaint();
                }
            }
        }

        if (UnCompressedPngFind.UnCompressed["Haven't Android Compression"] != 0) {
            if (GUILayout.Button("From iOS to Android")) {
                if (EditorUtility.DisplayDialog("WARNING", "Do you really want to recompress textures? This operation is not irreversible!", "Yes", "Cancel")) {
                    TextureCompressor.FromiOSToAndroidCompression(UnCompressedPngFind.UncompressedAndroidList);
                    Debug.LogFormat("<color=#00ffffff> Copmressing From iOS to Android was occured </color>");
                    FindUncompresedTextures();
                    Repaint();
                }
            }
        }

        if (UnCompressedPngFind.UnCompressed["Haven't any Compression"] != 0) {
            showPosition = EditorGUILayout.Foldout(showPosition, "Uncompressed textures");
            if (showPosition) {
                if (EditorGUILayout.BeginFadeGroup(uncompressTexturesExist)) {
                    //EditorGUILayout.BeginHorizontal();
                    EditorGUI.indentLevel++;
                    if (UnCompressedPngFind.TextureList.Count > 7) {
                        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(300), GUILayout.Height(110));
                        if (testBool1) {
                            foreach (var texture in UnCompressedPngFind.TextureList) {
                                testList.Add(EditorGUILayout.ObjectField(texture, typeof(UnityEngine.Object), true).GetInstanceID());// GetInstanceID());
                            }
                            testBool1 = false;
                            testBool2 = true;
                        } else {
                            foreach (var texture in UnCompressedPngFind.TextureList) {
                                EditorGUILayout.ObjectField(texture, typeof(UnityEngine.Object), true).GetInstanceID();
                            }
                        }

                        EditorGUILayout.EndScrollView();
                    } else {
                        foreach (var texture in UnCompressedPngFind.TextureList) {
                            EditorGUILayout.ObjectField(texture, typeof(UnityEngine.Object), true);
                        }
                    }
                    OnInspectorUpdate();

                    EditorGUI.indentLevel--;
                }
                // EditorUtility.FocusProjectWindow();
            }
        }

        GUILayout.Space(20);
        GUILayout.Label(content1, style);
        GUILayout.Space(10);

        if (FindPrefabsWithAssetBundlesNames()) {
            if (GUILayout.Button("Remove All AssetBundle Names", glo1)) {
                CreateBundles.RemoveNames();
            }
        }

        GUILayout.Space(10);

        if (GUILayout.Button("Build AssetBundles for iOS", glo2)) {
            if (EditorUtility.DisplayDialog("WARNING", "Do you really want to build AssetBundles for iOS? This operation can take a long time!", "Yes", "Cancel")) {
                Directory.CreateDirectory(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), "IOS"));
                BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/AssetBundles/IOS", BuildAssetBundleOptions.ChunkBasedCompression/*None*/, BuildTarget.iOS);
                this.Close();
            }
        }

        if (GUILayout.Button("Build AssetBundles for Android", glo2)) {
            if (EditorUtility.DisplayDialog("WARNING", "Do you really want to build AssetBundles for Android? This operation can take a long time!", "Yes", "Cancel")) {
                Directory.CreateDirectory(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetBundles"), "Android"));
                BuildPipeline.BuildAssetBundles("Assets/StreamingAssets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
                this.Close();
            }
        }

        if (GUILayout.Button("Build AssetBundles for Both", glo2)) {
            if (EditorUtility.DisplayDialog("WARNING", "Do you really want to build AssetBundles for both platforms?? This operation can take a long time!", "Yes", "Cancel")) {
                CreateBundles.BuildAllAssetBundlesBoth();
                this.Close();
            }
        }

        GUILayout.EndVertical();
        if (testBool2) {
            foreach (var ter in testList) {
                Debug.Log("=-=-=-=-=-=-=-" + ter);
            }
            testBool2 = false;
        }
    }

    public void OnInspectorUpdate() {
        this.Repaint();
    }
}
