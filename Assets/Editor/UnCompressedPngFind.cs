﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

public class UnCompressedPngFind : MonoBehaviour {

    private static int notCompressedNum;
    private static int notCompressedAndroidNum;
    private static int notCompressediOSNum;
    private static List<string> uncompressedList;
    private static List<string> uncompressedAndroidList;
    private static List<string> uncompressediOSList;
    private static List<Texture> textureList;
    private static Dictionary<string, int> unCompressed = new Dictionary<string, int>();
    private static bool findingEnd = false;

    public static Dictionary<string, int> UnCompressed {
        get { return unCompressed; }
        private set { unCompressed = value; }
    }

    public static bool FindingEnd {
        get { return findingEnd; }
        private set { findingEnd = value; }
    }

    public static List<string> UncompressedList {
        get { return uncompressedList; }
        private set { uncompressedList = value; }
    }

    public static List<string> UncompressedAndroidList {
        get { return uncompressedAndroidList; }
        private set { uncompressedAndroidList = value; }
    }

    public static List<string> UncompressediOSList {
        get { return uncompressediOSList; }
        private set { uncompressediOSList = value; }
    }

    public static List<Texture> TextureList {
        get { return textureList; }
        private set { textureList = value; }
    }

    public void StartFindMethod() {
        UnCompressedPngFind.FindUncompressedTextures();
    }

   // [MenuItem("Assets/Compressor/FindUncompressedTextures")]
    static void FindUncompressedTextures() {
        UncompressedList = new List<string>();
        UncompressedAndroidList = new List<string>();
        UncompressediOSList = new List<string>();
        TextureList = new List<Texture>();

        UnCompressed.Clear();
        findingEnd = false;

        notCompressedNum = 0;
        notCompressedAndroidNum = 0;
        notCompressediOSNum = 0;

        TextureImporterPlatformSettings tiPs1 = new TextureImporterPlatformSettings();
        TextureImporterPlatformSettings tiPs2 = new TextureImporterPlatformSettings();
        TextureImporter importer;
        var guids = AssetDatabase.FindAssets("t:Texture", null);

        int num = 0;

        Debug.LogFormat("<color=#0000ffff>=================LENGTH=============" + guids.Length + "</color>");

        foreach (var tex in guids) {
            try {
                importer = (TextureImporter)TextureImporter.GetAtPath(AssetDatabase.GUIDToAssetPath(tex));
                Debug.Log("------------gogogogogogogo-----------------");
                importer.GetPlatformTextureSettings("Android").CopyTo(tiPs1);
                importer.GetPlatformTextureSettings("iPhone").CopyTo(tiPs2); 
            }
            catch (InvalidCastException e) {
                Debug.LogFormat("<color=#ff0000ff>------Exception------ " + e + "</color>");
                continue;
            }
               
            if (!tiPs1.overridden && !tiPs2.overridden) {
                notCompressedNum++;
                num++;
                UncompressedList.Add(AssetDatabase.GUIDToAssetPath(tex));
                Debug.LogFormat("<color=#00ff00ff>" + num + " - " + AssetDatabase.GUIDToAssetPath(tex) + " - is haven't any compression!!! </color>");
                TextureList.Add((Texture)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(tex), typeof(Texture)));
            }
            else if (!tiPs1.overridden) {
                notCompressedAndroidNum++;
                num++;
                UncompressedAndroidList.Add(AssetDatabase.GUIDToAssetPath(tex));
                Debug.LogFormat("<color=#00ff00ff>" + num + " - " + AssetDatabase.GUIDToAssetPath(tex) + " - is haven't Android compression!!! </color>");
            }
            else if (!tiPs2.overridden) {
                notCompressediOSNum++;
                num++;
                UncompressediOSList.Add(AssetDatabase.GUIDToAssetPath(tex));
                Debug.LogFormat("<color=#00ff00ff>" + num + " - " + AssetDatabase.GUIDToAssetPath(tex) + " - is haven't iOS compression!!! </color>");
            }
        }

        UnCompressed.Add("Haven't any Compression", notCompressedNum);
        UnCompressed.Add("Haven't Android Compression", notCompressedAndroidNum);
        UnCompressed.Add("Haven't iOS Compression", notCompressediOSNum);

        FindingEnd = true;
    }
}
