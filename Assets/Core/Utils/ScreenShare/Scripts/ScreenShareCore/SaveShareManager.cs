﻿using UnityEngine;
using System.IO;

namespace Wear.Utils.CaptureAndShare {
    public class SaveShareManager {
        public static int CodeVersion = 102;

        /// <returns>Permission is Granted</returns>
        public static bool SavePhoto(byte[] subjectBytes, string fileAlbum, string filename, NativeGallery.MediaSaveCallback saveCallback)
        {
            var permission = NativeGallery.SaveImageToGallery(subjectBytes, fileAlbum, filename, saveCallback);

            return permission == NativeGallery.Permission.Granted;
        }

        /// <returns>Permission is Granted</returns>
        public static bool SavePhoto(string subjectPath, string fileAlbum, string filename, NativeGallery.MediaSaveCallback saveCallback)
        {
            return SavePhoto(File.ReadAllBytes(subjectPath), fileAlbum, filename, saveCallback);
        }

        /// <returns>Permission is Granted</returns>
        public static bool SavePhoto(Texture2D subjectTexture, string fileAlbum, string filename, NativeGallery.MediaSaveCallback saveCallback)
        {
            var permission = NativeGallery.SaveImageToGallery(subjectTexture, fileAlbum, filename, saveCallback);

            return permission == NativeGallery.Permission.Granted;
        }

        /// <returns>Permission is Granted</returns>
        public static bool SaveVideo(string subjectPath, string fileAlbum, string filename, NativeGallery.MediaSaveCallback saveCallback)
        {
            var permission = NativeGallery.SaveVideoToGallery(subjectPath, fileAlbum, filename, saveCallback);

            return permission == NativeGallery.Permission.Granted;
        }

        public static void ShareContent(string subjectPath) {
            if (string.IsNullOrEmpty(subjectPath) || string.IsNullOrWhiteSpace(subjectPath)) {
                return;
            }

            //NatShare.Share(subjectPath);
            var shareObject = new NativeShare().AddFile(subjectPath);
            shareObject.Share();
        }

        public static void ShareContent(string subjectPath, string text, string title) {
            if (string.IsNullOrEmpty(subjectPath) || string.IsNullOrWhiteSpace(subjectPath)) {
                return;
            }

            //NatShare.Share(subjectPath);
            var shareObject = new NativeShare().AddFile(subjectPath);
            shareObject.SetText(text);
            shareObject.SetTitle(title);
            shareObject.Share();
        }

        public static void ShareContent(string subjectPath, string subject, string target, string text, string title)
        {
            if (string.IsNullOrEmpty(subjectPath) || string.IsNullOrWhiteSpace(subjectPath)) {
                return;
            }

            var shareObject = new NativeShare().AddFile(subjectPath);
            shareObject.SetSubject(subject);
            shareObject.SetTarget(target);
            shareObject.SetText(text);
            shareObject.SetTitle(title);
            shareObject.Share();
        }
    }
}
