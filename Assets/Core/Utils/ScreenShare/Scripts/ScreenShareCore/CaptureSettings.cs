﻿using UnityEngine;
using System;


namespace Wear.Utils.CaptureAndShare
{
    [CreateAssetMenu(fileName = "CaptureSettings", menuName = "Wear/Core/Configs/CaptureSettings")]
    [Serializable]
    public class CaptureSettings : ScriptableObject
    {
        public const int CodeVersion = 102;

        public enum ResolutionMode { MultiplierScreenSize, FixedResolution, FixedWidth, FixedHeight, MaxWidth, MaxHeight, TargetPixelSquared }
        public enum AudioRecordMode { WithoutAudio, AudioListener, Microphone}
        public enum BitrateMode { TargetValue, Multiply_With_WidthHeight }

        [Serializable]
        public class SettingsData
        {
            [Header("Video Main")]
            public ResolutionMode resolutionMode = ResolutionMode.FixedResolution;
            public float width = 720;
            public float height = 1280;
            public AudioRecordMode audioRecordMode;
            //public bool recordUI = false;
            [Header("Video Quality"),Space(10)] public BitrateMode bitrateMode = BitrateMode.TargetValue;
            [ Tooltip("TARGET Bitrate in bits.")] public float targetBitrate = 32e6f;
            [Range(4,144), Tooltip("HIGHER values = LOWER final bitrate")]
            public int framerateParam = 144;
            public int keyframes = 3;
            public int writeFrameCounter = 1;
            [Header("Microphone settings")]
            public string MicDevice = "";
            public int SampleRate = 44100;
            [Header("File naming, paths"), Space(10), Tooltip("Temp path in persistentDataPath")] public string tempMediaPath = "mediaTemp";
            public string AlbumName = "WearApplication";
            public string FileNamePrefix = "WearMediaContent";

            public Vector2Int GetRecordResolution()
            {
                var mainDataV = this;

                var result = new Vector2();
                float factor = 0;

                switch (mainDataV.resolutionMode)
                {
                    case ResolutionMode.FixedResolution:
                        result.Set(mainDataV.width, mainDataV.height);
                        break;
                    case ResolutionMode.MultiplierScreenSize:
                        result.Set(mainDataV.width * Screen.width, mainDataV.height * Screen.height);
                        break;
                    case ResolutionMode.MaxWidth:
                        factor = Mathf.Clamp(Screen.width / mainDataV.width, 0.0f, 1f);
                        result.Set(mainDataV.width * factor, mainDataV.height * factor);
                        break;
                    case ResolutionMode.MaxHeight:
                        factor = Mathf.Clamp(Screen.height / mainDataV.height, 0.0f, 1f);
                        result.Set(mainDataV.width * factor, mainDataV.height * factor);
                        break;
                    case ResolutionMode.FixedWidth:
                        result.Set(mainDataV.width, Screen.height * (mainDataV.width / Screen.width));
                        break;
                    case ResolutionMode.FixedHeight:
                        result.Set(Screen.width * (mainDataV.height / Screen.height), mainDataV.height);
                        break;
                    case ResolutionMode.TargetPixelSquared:
                        factor = (mainDataV.height * mainDataV.width) / (Screen.width * Screen.height);
                        result.Set(Screen.width * factor, factor * Screen.height);
                        break;
                }

                var final = new Vector2Int();

                final.x = (int)Mathf.Clamp(result.x, 2, 10000);
                if (final.x % 2 != 0)
                {
                    final.x += 1;
                }

                final.y = (int)Mathf.Clamp(result.y, 2, 10000);
                if (final.y % 2 != 0)
                {
                    final.y += 1;
                }

                final.Set(Mathf.Clamp(final.x, 2, 10000), Mathf.Clamp(final.y, 2, 10000));
                return final;
            }

            public int GetBitrate()
            {
                var mainDataV = this;

                switch (mainDataV.bitrateMode)
                {
                    case BitrateMode.TargetValue:
                        return (int)mainDataV.targetBitrate; break;
                    case BitrateMode.Multiply_With_WidthHeight:
                        var res = GetRecordResolution();
                        return (int)(mainDataV.targetBitrate * res.x * res.y);
                        break;
                }

                return 1000;
            }
        }

        public static CaptureSettings GetDefault()
        {
            CaptureSettings NewSet = new CaptureSettings();
            NewSet.mainData = new SettingsData()
            {
                resolutionMode = ResolutionMode.MultiplierScreenSize,
                width = 0.85f,
                height = 0.85f,
                audioRecordMode = AudioRecordMode.AudioListener,
                bitrateMode = BitrateMode.TargetValue,
                targetBitrate = 37e6f,
                framerateParam = 144,
                keyframes = 3,
                tempMediaPath = "TEMP",
                AlbumName = "WearApplication",
                FileNamePrefix = "WearMediaContent"

            };

            return NewSet;
        }

        public SettingsData mainData;
    }
}
