﻿using UnityEngine;

namespace Wear.Utils.CaptureAndShare
{
    [RequireComponent(typeof(CanvasGroup))]
    public class HideWhenCapture : MonoBehaviour
    {
        public const int CodeVersion = 102;

        private void Awake() {
            CaptureCore.OnScreenshotCapturingStart += Disable;
            CaptureCore.OnScreenshotCapturingEnd += Enable;
        }

        private void OnDestroy() {
            CaptureCore.OnScreenshotCapturingStart -= Disable;
            CaptureCore.OnScreenshotCapturingEnd -= Enable;
        }

        void Disable() {
            GetComponent<CanvasGroup>().alpha = 0;
        }

        void Enable() {
            GetComponent<CanvasGroup>().alpha = 1;
        }
    }
}
