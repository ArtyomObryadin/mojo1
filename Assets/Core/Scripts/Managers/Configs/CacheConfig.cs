﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wear.ARCompanyCore {
    [CreateAssetMenu(fileName = "CacheConfig", menuName = "Wear/Core/Configs/Cache Config")]
    public class CacheConfig : ScriptableObject {
        [Header("Main Content")]
        [Tooltip("Video Content Path")]
        public string VideoCachePath;
        [Tooltip("Effects Path")]
        public string EffectsCachePath;

        [Header("Prefix")]
        [Tooltip("Video Content Info FileName")]
        public string VideoInfoName;
        [Tooltip("Effects Content Info FileName")]
        public string EffectsInfoName;
    }
}
