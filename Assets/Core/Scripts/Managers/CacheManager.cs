﻿using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using UniRx.Async;

namespace Wear.ARCompanyCore.Managers {
    [System.Serializable]
    public class CacheInfo {
        public string Name;
        public string LastModifiedData;
        public string CachePath;
        public string Prefix;
    }

    public interface ICacheable {
        string CacheId { get; }
        CacheInfo CacheInfo { get; set; }
    }

    public class CacheManager : MonoBehaviour {
        
        public CacheConfig cacheConfig;
        
        public static string cacheProductPath;
        public static string cacheCategoryPath;
        static string _baseCachePath;

        public static string BaseCachePath {
            get => string.IsNullOrEmpty(_baseCachePath) ? throw new System.Exception("Cache Path not initilized!!!") : _baseCachePath;
            private set => _baseCachePath = value;
        }

        private void Start() { }

        public static void Init(string basecachepath) {
            BaseCachePath = basecachepath;
        }

        public static string GetInfoFilePath (ContentType type) {
            if (type == ContentType.Video) {
                return BaseCachePath + Manager.CacheManager.cacheConfig.VideoInfoName;
            }
            else {
                return BaseCachePath + Manager.CacheManager.cacheConfig.EffectsInfoName;
            }
        }

        public static void CacheObject<T>(T obj, CacheInfo cacheInfo, bool isNeedDelete = false) {

            Debug.Log("<<-- Must be cached -->> " + cacheInfo.Name + " ======== " + BaseCachePath);
            BinaryFormatter bf = new BinaryFormatter();
            if (!Directory.Exists(BaseCachePath + cacheInfo.CachePath)) { //"SendedMessages"
                Directory.CreateDirectory(BaseCachePath + cacheInfo.CachePath);
            }

            string path = BaseCachePath + cacheInfo.CachePath;

            if (Directory.Exists(path)) {
                if (isNeedDelete) {
                    DirectoryInfo dirInfo = new DirectoryInfo(path);
                    foreach (FileInfo file in dirInfo.GetFiles()) { file.Delete(); }
                }
                using (FileStream fstream = File.Create(path + "/" + cacheInfo.Name)) {
                    try {
                        bf.Serialize(fstream, obj);
                    }
                    catch (SerializationException e) {
                        throw;
                    }
                    finally {
                        fstream.Close();
                    }
                }
            }
        }

        public static void CacheObjectList<T>(List<ICacheable> objectsforcache)
        {
            string cachePath;
            if (objectsforcache != null && objectsforcache.Count > 0) {
                cachePath = BaseCachePath + objectsforcache[0]?.CacheInfo.CachePath;
            }
            else {
                return;
            }

            RevalidateCacheByPath(cachePath, objectsforcache.Select(x => x.CacheInfo).ToList());

            List<ICacheable> notValidObjs = new List<ICacheable>();
            foreach (var obj in objectsforcache) {
                if (!IsObjectValid(obj.CacheInfo).Result) {
                    notValidObjs.Add(obj);
                    UnityEngine.Debug.LogFormat("<color=#ff0000ff> NotValid </color>");
                }
            }

            CacheObjectCollection<T>(notValidObjs);
        }

        static void CacheObjectCollection<T>(List<ICacheable> objectsforcache)
        {
            BinaryFormatter bf = new BinaryFormatter();
            string cachePath;
            if (objectsforcache != null && objectsforcache.Count > 0) {
                cachePath = BaseCachePath + objectsforcache[0]?.CacheInfo.CachePath;
            }
            else {
                return;
            }

            if (!Directory.Exists(cachePath)) {
                Directory.CreateDirectory(cachePath);
            }

            foreach (ICacheable obj in objectsforcache) {
                if (Directory.GetFiles(cachePath).FirstOrDefault(x => new FileInfo(x).Name.StartsWith(obj.CacheId.ToString())) != null) {
                    string path = Path.GetFullPath(Directory
                        .GetFiles(cachePath)
                        .FirstOrDefault(x => new FileInfo(x).Name.StartsWith(obj.CacheId.ToString()))).Replace('\\', '/');

                    File.Delete(path);
                }
                using (FileStream fstream = File.Create(cachePath + "/" + obj.CacheId + "_" + obj.CacheInfo.LastModifiedData)) {
                    bf.Serialize(fstream, (T)obj);
                    fstream.Close();
                }
            }
        }

        public static List<T> GetObjectsCollection<T>(string path) {
            List<T> typeList = new List<T>();

            if (Directory.Exists(BaseCachePath + path)) {
                BinaryFormatter bf = new BinaryFormatter();

                foreach (var cat in Directory.GetFiles(BaseCachePath + path)) {

                    FileStream fstream = File.Open(cat, FileMode.Open);
                    typeList.Add((T)bf.Deserialize(fstream));
                    fstream.Dispose();
                }
            }

            return typeList;
        }

        public static void CacheObjectByType<T>(ICacheable obj) {
            string cachePath;

            BinaryFormatter bf = new BinaryFormatter();

            cachePath = BaseCachePath + obj.CacheInfo.CachePath;

            RestructCache(obj);

            if (!Directory.Exists(cachePath)) {
                Directory.CreateDirectory(cachePath);
            }

            if (Directory.Exists(cachePath)) {
                DirectoryInfo info = new DirectoryInfo(cachePath);
                var files = info.GetFiles();

                if (files != null && files.Length != 0) {
                    foreach (var file in files) {
                        if (file.Name.Contains(obj.CacheId)) {
                            file.Delete();
                        }
                    }
                }

                using (FileStream fstream = File.Create(cachePath + obj.CacheId + "_" + System.DateTime.UtcNow.ToString("ddMMyyyyHHmm") /*obj.CacheInfo.LastModifiedData*/)) {
                    bf.Serialize(fstream, (T)obj);
                    fstream.Close();
                }
            }
        }

        public static void CacheRegObjectByType<T>(ICacheable obj)
        {
            string cachePath;

            BinaryFormatter bf = new BinaryFormatter();
            cachePath = BaseCachePath + obj.CacheInfo.CachePath;

            if (!Directory.Exists(cachePath)) {
                Directory.CreateDirectory(cachePath);
            }

            //RegistrationsParameterScript temp = obj as RegistrationsParameterScript;

            if (Directory.Exists(cachePath)) {
                if (File.Exists(cachePath)) {
                    File.Delete(cachePath);
                }
                using (FileStream fstream = File.Create(cachePath + obj.CacheInfo.Name)) {
                    bf.Serialize(fstream, (T)obj);
                    fstream.Close();
                }
            }
        }

        public static void CacheBundleDate(string date, string folderPath)
        {
            BinaryFormatter bf = new BinaryFormatter();

            if (!Directory.Exists(folderPath)) {
                Directory.CreateDirectory(folderPath);
            }

            string path = Path.Combine(folderPath, "date");
            if (Directory.Exists(folderPath)) {
                if (File.Exists(path)) {
                    File.Delete(path);
                }

                using (FileStream fstream = File.Create(path)) {
                    bf.Serialize(fstream, date);
                    fstream.Close();
                }
            }
        }

        public static string GetBundleDate(string folderName)
        {
            string dirPath = BaseCachePath + folderName;
            string filePath = dirPath + "date";

            string obj = "";

            if (Directory.Exists(dirPath)) {
                if (File.Exists(filePath)) {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream fstream = File.Open(filePath, FileMode.Open);
                    obj = (string)bf.Deserialize(fstream);
                    fstream.Dispose();
                }
            }

            return obj;
        }

        public static string GetEffectPathByName(string name) {
            string dirPath = BaseCachePath + Manager.CacheManager.cacheConfig.EffectsCachePath;
            string filePath = dirPath + name;

            if (Directory.Exists(dirPath)) {
                if (File.Exists(filePath)) {
                    return filePath;
                }
            }

            return "";
        }

        public static bool CheckCachedFolder(string path)
        {
            string folderPath = BaseCachePath + path;

            if (!Directory.Exists(folderPath)) {
                return false;
            }
            else {
                DirectoryInfo dirInfo = new DirectoryInfo(folderPath);
                var files = dirInfo.GetFiles();
                if (files != null && files.Length > 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        //public static void AddContentData<T>(T contentData, ContentType type) {
        //    string dirName = BaseCachePath + Manager.CacheManager.cacheConfig.;
        //    string fileName = (type == ContentType.Video ? Manager.CacheManager.cacheConfig.VideoInfoName : Manager.CacheManager.cacheConfig.EffectsInfoName) + Manager.CacheManager.cacheConfig.InfoDataFileName;
        //    if (CheckCachedFileByName(dirName, fileName)) {Manager.CacheManager

        //    }
        //}

        public static void CheckCreateCachedFolder(string folderName)
        {
            if (!Directory.Exists(BaseCachePath + folderName)) {
                Directory.CreateDirectory(BaseCachePath + folderName);
            }
        }

        public static bool CheckCachedFile(string folderName)
        {
            if (Directory.Exists(BaseCachePath + folderName)) {
                DirectoryInfo dirInfo = new DirectoryInfo(BaseCachePath + folderName);
                if (dirInfo.GetFiles().Length != 0) { return true; }
                else { return false; }
            }
            else {
                return false;
            }
        }

        public static bool CheckCachedFileByName(string folderName, string fileName) {
            if (CheckCachedFile(folderName)) {
                DirectoryInfo dirInfo = new DirectoryInfo(BaseCachePath + folderName);
                foreach (var obj in dirInfo.GetFiles()) {
                    if (obj.Name.Contains(fileName)) {
                        return true;
                    }
                }
            }
            
            return false;
        }

        public static void CheckFileByInfoAndDelete(CacheInfo cacheInfo) {
            if (CheckCachedFile(cacheInfo.CachePath)) {
                DirectoryInfo dirInfo = new DirectoryInfo(BaseCachePath + cacheInfo.CachePath);
                foreach (var obj in dirInfo.GetFiles()) {
                    if (obj.Name.Contains(cacheInfo.Name)) {
                        obj.Delete();
                        break;
                    }
                }
            }
        }

        public static void CheckFileByInfoAndDelete(string folderName, string fileName) {
            if (CheckCachedFile(folderName)) {
                DirectoryInfo dirInfo = new DirectoryInfo(BaseCachePath + folderName);
                foreach (var obj in dirInfo.GetFiles()) {
                    if (obj.Name.Contains(fileName)) {
                        obj.Delete();
                        break;
                    }
                }
            }
        }

        public static void DeleteCacheInFolder(string folderName, bool isDelFolder = true) {
            string path = BaseCachePath + folderName;
            if (Directory.Exists(path)) {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                foreach (FileInfo file in dirInfo.GetFiles()) { file.Delete(); }
                foreach (DirectoryInfo subDirectory in dirInfo.GetDirectories()) { subDirectory.Delete(true); }

                if (isDelFolder) {
                    Directory.Delete(path);
                }
            }
        }

        static string GetRightFileName(string path, string id)
        {
            string rightFileName = "";
            if (Directory.Exists(path)) {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                foreach (FileInfo file in dirInfo.GetFiles()) {
                    if (file.Name.Contains(id)) {
                        rightFileName = file.Name;
                    }
                }
            }
            return rightFileName;
        }

        public static T GetObjectByType<T>(string folderName, string fileName) {
            string dirPath = BaseCachePath + folderName;
            string filePath = dirPath + "/" + fileName;

            T obj = default(T);

            if (CheckCachedFile(folderName)) {
                string rightFileName = GetRightFileName(dirPath, fileName);
                filePath = dirPath + "/" + (string.IsNullOrEmpty(rightFileName) ? fileName : rightFileName);
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fstream = File.Open(filePath, FileMode.Open);
                obj = (T)bf.Deserialize(fstream);
                fstream.Dispose();
            }

            return obj;
        }

        public static T GetObjectByType<T>(CacheInfo cacheInfo) where T : new () {
            string filePath = GetCachePathByCacheInfo(cacheInfo);
            string dirPath = cacheInfo.CachePath;

            Debug.Log("FileName ---> " + filePath + " --- dirPath --- " + dirPath);

            T obj = new T();// default(T);
            if (CheckCachedFileByName(dirPath, cacheInfo.Name)) {
                //Debug.Log("--------------------------------------------------------");
                BinaryFormatter bf = new BinaryFormatter();
                FileStream fstream = File.Open(filePath, FileMode.Open);
                obj = (T)bf.Deserialize(fstream);
                fstream.Dispose();
                return obj;
            }
            //Debug.Log("----------------------werwerewrewrewr----------------------------------");

            return obj;
        }

        public static T GetObjectByType<T>(ICacheable cacheInfo) {
            T obj = default(T);
            if (null != cacheInfo) {

                string filePath = GetCachePathByCacheInfo(cacheInfo?.CacheInfo);

                string dirPath = BaseCachePath + cacheInfo.CacheInfo.CachePath;

                //if (Directory.Exists(dirPath)) {
                if (CheckCachedFileByName(cacheInfo.CacheInfo.CachePath, cacheInfo?.CacheInfo.Name)) {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream fstream = File.Open(filePath, FileMode.Open);
                    obj = (T)bf.Deserialize(fstream);
                    fstream.Dispose();
                }

                Debug.Log("All work fine");
                return obj;
            }
            else {
                Debug.Log("Something went wrong");
                return obj;
            }
        }

        /// <summary>
        /// Call this method for delete old objects in cache,that not contains in new objects collection
        /// </summary>
        /// <param name="objects"></param>
        static void RestructCache(ICacheable cacheobj)
        {
            string cachePath;
            if (cacheobj != null) {
                cachePath = BaseCachePath + cacheobj?.CacheInfo.CachePath;
                //Debug.Log(cacheobj?.CacheInfo.CachePath);
            }
            else {
                return;
            }

            if (!Directory.Exists(cachePath)) { return; }

            foreach (string filename in Directory.GetFiles(cachePath)) {
                if ((new FileInfo(filename).Name).Contains(cacheobj.CacheId.ToString())) {
                    string path = Path.GetFullPath(filename).Replace('\\', '/');
                    Debug.Log(path);
                    File.Delete(path);
                }
            }
        }

        /// <summary>
        /// Revalidate Cache by path
        /// </summary>
        /// <param name="cachepath"></param>
        /// <param name="objectsForRevalidate">objects info-list for revalidate</param>
        public static void RevalidateCacheByPath(string cachepath, List<CacheInfo> objectsForRevalidate)
        {

            if (!Directory.Exists(cachepath)) return;

            foreach (string dirname in Directory.GetDirectories(cachepath)) {
                Debug.Log(dirname);
                if (objectsForRevalidate.FirstOrDefault(x => new DirectoryInfo(dirname).Name.StartsWith(x.Name.ToString())) == null) {
                    string path = Path.GetFullPath(dirname).Replace('\\', '/');

                    DirectoryInfo dirInfo = new DirectoryInfo(dirname);
                    foreach (FileInfo file in dirInfo.GetFiles()) file.Delete();
                    foreach (DirectoryInfo subDirectory in dirInfo.GetDirectories()) subDirectory.Delete(true);
                    Directory.Delete(path);
                }
            }
        }

        public static void MoveFilesToFolder(string oldFold, string newFold)
        {

            List<string> oldPaths = new List<string>();
            List<string> newPaths = new List<string>();
            //oldPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ReceivedContentCachePath + oldFold);
            //oldPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ProfileMessageCachePath + "senderMessages/" + oldFold);
            //oldPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ProfileMessageCachePath + "receiverMessages/" + oldFold);

            //newPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ReceivedContentCachePath + newFold);
            //newPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ProfileMessageCachePath + "senderMessages/" + newFold);
            //newPaths.Add(BaseCachePath + Managers.CacheManager.cacheConfig.ProfileMessageCachePath + "receiverMessages/" + newFold);

            for (int i = 0; i < oldPaths.Count; i++) {
                if (Directory.Exists(oldPaths[i])) {
                    DirectoryCopy(oldPaths[i], newPaths[i]);

                    Directory.Delete(oldPaths[i], true);
                }
            }
        }

        static void DirectoryCopy(string oldFolder, string newFolder)
        {
            //Managers.UIManager.LoadingContentPanel(true, "Data is copied");
            string[] files = Directory.GetFiles(oldFolder);
            string[] directories = Directory.GetDirectories(oldFolder);

            bool isFolderExist = false;

            if (!Directory.Exists(newFolder)) {
                Directory.CreateDirectory(newFolder);
            }
            else {
                isFolderExist = true;
            }

            if (files != null && files.Length > 0) {
                foreach (var elemF in files) {
                    SendFile(elemF, newFolder.Replace("\\", "/"));
                }
            }

            if (directories != null && directories.Length > 0) {
                string tempFolder = newFolder;

                foreach (var elem in directories) {
                    newFolder = tempFolder + elem.Replace(oldFolder, "");
                    DirectoryCopy(elem.Replace("\\", "/"), newFolder);
                }
            }

            //Managers.UIManager.LoadingContentPanel(false);
        }

        static void SendFile(string fileUrl, string pathFolder)
        {

            string path = fileUrl.Replace("\\", "/");
            FileInfo info = new FileInfo(path);
            string fileName = info.Name;

            if (!File.Exists(pathFolder + "/" + fileName)) {
                File.Move(path, pathFolder + "/" + fileName);
            }
        }

        public static async Task<bool> IsObjectValid(CacheInfo cacheInfo)
        {
            string path = Application.persistentDataPath + cacheInfo.CachePath + cacheInfo.Name;// + Transliteration.FofrmatDateString(cacheInfo.LastModifiedData);
            bool isvalid = File.Exists(path);

            return isvalid;
        }

        /// <summary>
        /// This method for get fuul path to object by cache info
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string GetCachePathByCacheInfo(CacheInfo info)
        {
            return BaseCachePath + info.CachePath + info.Name; // + Transliteration.FofrmatDateString(info.LastModifiedData);
        }

        public static async void CacheObjectByBytes(CacheInfo cacheInfo, byte[] bytes)
        {
            if (!Directory.Exists(Application.persistentDataPath + cacheInfo.CachePath)) {  //TODO Check Path
                Directory.CreateDirectory(Application.persistentDataPath + cacheInfo.CachePath);
            }
            string path = Application.persistentDataPath + cacheInfo.CachePath + cacheInfo.Name;// + CacheManager.FormatDateString(cacheInfo.LastModifiedData);

            if (File.Exists(path)) { return; }

            using (FileStream fstream = File.Create(path)) {
                fstream.Write(bytes, 0, bytes.Length);
                fstream.Close();
            }
        }


        public static void RevalidateAllCacheByVersion(float version)
        {
            if (PlayerPrefs.GetFloat("cacheversion") != version) {
                DeleteAllCache();
                PlayerPrefs.SetFloat("cacheversion", version);
            }
        }

        static void DeleteAllCache()
        {
            string path = BaseCachePath;
            if (Directory.Exists(path)) {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                foreach (FileInfo file in dirInfo.GetFiles()) file.Delete();
                foreach (DirectoryInfo subDirectory in dirInfo.GetDirectories()) subDirectory.Delete(true);
                Directory.Delete(path);
            }
        }

        public static string FormatDateString(string name)
        {
            char[] replaceChars = new char[] { ' ', '"', '*', '+', ',', '(', ')', '-', '.', 'T', '/', '\\', ':' };
            string frmtString = name ?? "";
            foreach (var ch in replaceChars) {
                frmtString = frmtString.Replace(ch, '_');
            }

            frmtString = Regex.Replace(frmtString, "_+", "");
            frmtString = frmtString.ToLower();

            return frmtString;
        }

        private void CheckExpiredMessages()
        {
            string path = BaseCachePath;// + cacheConfig.regFileCachePath;
            if (Directory.Exists(path)) {
                DirectoryInfo dir = new DirectoryInfo(path);
                foreach (DirectoryInfo groupDir in dir.GetDirectories()) {
                    foreach (DirectoryInfo messageDir in groupDir.GetDirectories()) {
                        foreach (FileInfo file in messageDir.GetFiles()) {
                            if (file.Name.Contains(messageDir.Name)) {
                                string fileName = file.Name;
                                System.DateTime date = GetFileCacheDate(fileName);
                                System.TimeSpan diff = System.DateTime.UtcNow.Subtract(date);

                                if ((float)diff.TotalMinutes > 24f) {
                                    messageDir.Delete(true);
                                }
                            }
                        }
                    }
                }
            }
        }

        public System.DateTime GetFileCacheDate(string fileName)
        { // Get Send Data from proilemessgroup file!!!
            string fileDate = "";
            bool isDate = false;
            foreach (var ch in fileName) {
                if (isDate) {
                    fileDate += ch;
                }

                if (ch == '_') { isDate = true; }
            }

            System.DateTime result = System.DateTime.ParseExact(fileDate, "ddMMyyyyHHmm",
                                                 System.Globalization.CultureInfo.InvariantCulture);

            return result;
        }
    }
}
