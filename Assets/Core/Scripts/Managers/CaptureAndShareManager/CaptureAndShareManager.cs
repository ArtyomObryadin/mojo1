﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniRx.Async;
using UnityEngine;
using Wear.Utils;

#if PLATFORM_ANDROID && !UNITY_EDITOR
using UnityEngine.Android;
#endif

namespace Wear.ARCompanyCore {
    public interface ICaptureAndShareManager
    {
        Task Initialize();
    }

    public class CaptureAndShareManager : ICaptureAndShareManager
    {
        public async Task Initialize()
        {
            //IPHelper.InitializeIPAderess();

#if PLATFORM_ANDROID && !UNITY_EDITOR
            if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
            {
                Permission.RequestUserPermission(Permission.Microphone);
            }
#elif PLATFORM_IOS
            await MicrophoneInit();
#endif
            await Task.Delay(100);
            Debug.Log("Capture and share initialized.");
        }

#if UNITY_IOS
        private async Task MicrophoneInit()
        {
            await Application.RequestUserAuthorization(UserAuthorization.Microphone);
            if (Application.HasUserAuthorization(UserAuthorization.Microphone)) {
                Debug.Log("Microphone found");
            } else {
                Debug.Log("Microphone not found");
            }
        }
#endif
    }
}
