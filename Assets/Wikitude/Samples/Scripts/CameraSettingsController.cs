﻿using UnityEngine;
using UnityEngine.UI;
using Wikitude;
using System.Collections;
using UnityEngine.Video;
using UnityEngine.Android;
using Wear.ARCompanyCore.Managers;
using Wear.ARCompanyCore;

public class CameraSettingsController : SampleController
{
    public DeviceCamera Camera;
    public Button FlashModeBtn;
    public Camera arCamera;

    public static string name;

    private bool _suppressErrors;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1.5F);
        Camera.FocusMode = (CaptureFocusMode)2;
    }

    public void OnCameraOpened()
    {
        /* After the camera was opened, check what capabilities it has, to make sure that only the appropriate controls are displayed. */

        /* When checking if a certain camera feature is supported, the SDK will report an error if it is not, indicating the reason.
         * Since we are not interested in the reason, we temporarily suppress error handling until the end of the method.
         */
        _suppressErrors = true;

        if (Camera.IsFlashModeSupported(CaptureFlashMode.On))
        {
            FlashModeBtn.interactable = true;
        }
        else
        {
            FlashModeBtn.interactable = false;
        }

        StartCoroutine(EnableErrors());
    }

    private IEnumerator EnableErrors()
    {
        /* Because errors are reported in the next frame, wait a frame before enabling the errors back. */
        yield return null;
        _suppressErrors = false;
    }

    public void OnPositionChanged()
    {
        Manager.ContentManager.IsFrontCameraOn = (int)Camera.DevicePosition != 1;
        Camera.DevicePosition = (int)Camera.DevicePosition == 1 ? (CaptureDevicePosition)0 : (CaptureDevicePosition)1;
    }

    public void OnAutoFocusChanged(int newAutoFocus)
    {
        Camera.AutoFocusRestriction = (CaptureAutoFocusRestriction)newAutoFocus;
    }

    public void OnFlashModeChanged()
    {
        Manager.UIManager.IsFlashOn = (int)Camera.FlashMode != 1;
        Camera.FlashMode = (int)Camera.FlashMode == 1 ? (CaptureFlashMode)0 : (CaptureFlashMode)1;
    }

    public void OnZoomLevelChanged(float newZoomLevel)
    {
        Camera.ZoomLevel = newZoomLevel * (Camera.MaxZoomLevel - 1.0f) + 1.0f;
    }

    public void OnManualFocusChanged(float manualFocus)
    {
        Camera.ManualFocusDistance = manualFocus;
    }

    public void OnBackgroundClicked()
    {
        /* The background is a UI component that covers the entire screen and is triggered when no other control was pressed.
         * It is used to trigger focus and expose at point of interest commands.
         */
        /* When checking if a certain camera feature is supported, the SDK will report an error if it is not, indicating the reason.
         * Since we are not interested in the reason, we temporarily suppress error handling until the end of the method.
         */
        _suppressErrors = true;
        bool isFocusAtPointOfInterestSupported = Camera.IsFocusAtPointOfInterestSupported;
        //bool isExposeAtPointOfInterestSupported = Camera.IsExposeAtPointOfInterstSupported;

        StartCoroutine(EnableErrors());
    }

    public override void OnCameraError(Error error)
    {
        if (!_suppressErrors)
        {
            /* Log the error to the on-screen console */
            base.OnCameraError(error);
        }
    }
    [SerializeField] private GameObject forActive;
    GameObject contentContainer, wikitudeTargetObj;
    GameObject plane;
    public async void OnTargetFound(ImageTarget target)
    {
        name = target.Name;
        if (contentContainer != null)
        {
            Destroy(contentContainer);
        }
        contentContainer = Instantiate(forActive);
        plane = contentContainer.GetComponentInChildren<MeshCollider>().gameObject;
        if (Manager.ContentManager.IsFrontCameraOn)
        {
            plane.transform.localScale = new Vector3(-plane.transform.localScale.x, plane.transform.localScale.y, plane.transform.localScale.z);
        }
        wikitudeTargetObj = target.Drawable;
        Manager.ContentManager.TargetRecognized(contentContainer, target.Name);
    }
    private void Update()
    {
        if (contentContainer != null && wikitudeTargetObj != null)
        {
            contentContainer.transform.position = wikitudeTargetObj.transform.position;
            contentContainer.transform.localRotation = wikitudeTargetObj.transform.localRotation;
        }
    }
    public void OnTargetLost(ImageTarget target)
    {
        contentContainer.GetComponent<VideoContentController>().OnMarkerLost();
        contentContainer.SetActive(false);
        wikitudeTargetObj = null;
        Manager.ContentManager.TargetLost(target.Drawable, target.Name);
    }
}
